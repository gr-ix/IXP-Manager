<?php namespace IXP\Utils\Export;

/*
 * This file is managed by puppet, DO NOT EDIT BY HAND!
 */

use OSS_Array;

/*
 * Copyright (C) 2009-2016 Internet Neutral Exchange Association Company Limited By Guarantee.
 * All Rights Reserved.
 *
 * This file is part of IXP Manager.
 *
 * IXP Manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version v2.0 of the License.
 *
 * IXP Manager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License v2.0
 * along with IXP Manager.  If not, see:
 *
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

use stdClass;

use IXP\Exceptions\Utils\ExportException;

use IXP\Models\{
    Customer,
    Infrastructure,
    NetworkInfo,
    Router,
    SwitchPort,
    PhysicalInterface,
    Layer2Address,
    MacAddress
};
/**
 * JSON Schema Exporter
 *
 * Usage:
 *
 *     $jexport = new \IXP\Utils\Export\JsonSchema;
 *     $json_schema = $jexport->get( \IXP\Utils\Export\JsonSchema::EUROIX_JSON_LATEST );
 *
 * @see        https://github.com/euro-ix/json-schemas
 * @author     Nick Hilliard <nick@foobar.org>
 * @author     Barry O'Donovan <barry@opensolutions.ie>
 * @author     Zenon Mousmoulas <zmousm@noc.grnet.gr>
 * @author     Athanasios Douitsis <aduitsis@noc.ntua.gr>
 * @copyright  Copyright (C) 2009-2016 Internet Neutral Exchange Association Company Limited By Guarantee
 * @license    http://www.gnu.org/licenses/gpl-2.0.html GNU GPL V2.0
 */
class JsonSchemaGrix
{
    // Supported versions:
    // ended 201705: const EUROIX_JSON_VERSION_0_3 = "0.3";
    // ended 201705: const EUROIX_JSON_VERSION_0_4 = "0.4";
    // ended 201705: const EUROIX_JSON_VERSION_0_5 = "0.5";
    public const EUROIX_JSON_VERSION_0_6 = "0.6";
    public const EUROIX_JSON_VERSION_0_7 = "0.7";
    public const EUROIX_JSON_VERSION_1_0 = "1.0";
    // adding a new version? update sanitiseVersion() below also!

    public const EUROIX_JSON_LATEST = self::EUROIX_JSON_VERSION_1_0;

    public const EUROIX_JSON_VERSIONS = [
        self::EUROIX_JSON_VERSION_0_6,
        self::EUROIX_JSON_VERSION_0_7,
        self::EUROIX_JSON_VERSION_1_0,
    ];

    /**
     * Get the JSON schema (for a given version or for the latest version)
     *
     * @param string|null   $version    The version to get (or, if null / not present then the latest)
     * @param bool          $asArray    Do not convert to JSON but rather return the PHP array
     * @param bool          $detailed   Create the very detailed version (usually for logged in users)
     * @param bool          $tags       Include customer tags
     * @return string|array
     * @throws
     */
    public function get()
    {

        // normalise times to UTC for exports
        date_default_timezone_set('UTC');
        return [ 'ixpmanager_export' => [ 'timestamp' => date( 'Y-m-d', time() ) . 'T' . date( 'H:i:s', time() ) . 'Z', 'ixp_list' => [ $this->getIXPInfo() ], 'member_list' => $this->getMemberInfo('null',true,false) ] ];
    }

    /**
     * Ensure a given version exists or default to latest
     *
     * @param string $version Version string to sanitise
     *
     * @return string Sanitised version
     */
    public function sanitiseVersion( string $version ): string
    {
        if( in_array( $version, self::EUROIX_JSON_VERSIONS ) ) {
            return $version;
        }

        return self::EUROIX_JSON_LATEST;
    }

    /**
     * Collate the IXP specific information for the JSON schema export
     *
     * @param string $version The version to collate the detail for
     *
     * @return array
     *
     * @throws
     */
    private function getIXPInfo( $version = null )
    {
        $ixpinfo = [];

        $ixpinfo['shortname'] = config( 'identity.orgname'          );
        $ixpinfo['name']      = config( 'identity.legalname'        );
        $ixpinfo['country']   = config( 'identity.location.country' );
        $ixpinfo['url']       = config( 'identity.corporate_url'    );

        $ixpinfo['ixf_id'] = intval( config( 'identity.ixfid' ) );
        $ixpinfo['ixp_id'] = 1;                            // referenced in member's connections section

        $ixp_info['support_email']             = config( 'identity.support_email' );
        $ixp_info['support_phone']             = config( 'identity.support_phone' );
        $ixp_info['support_contact_hours']     = config( 'identity.support_hours' );

        $ixp_info['emergency_email']           = config( 'identity.support_email' );
        $ixp_info['emergency_phone']           = config( 'identity.support_phone' );
        $ixp_info['emergency_contact_hours']   = config( 'identity.support_hours' );
        $ixp_info['billing_contact_hours']     = config( 'identity.billing_hours' );

        $ixp_info['billing_email']             = config( 'identity.billing_email' );
        $ixp_info['billing_phone']             = config( 'identity.billing_phone' );

        #$ixp_info['peering_policy_list'] = array_values( \Entities\Customer::$PEERING_POLICIES );

        //$ixpinfo['vlans']   = d2r( 'NetworkInfo' )->asVlanEuroIXExportArray();
        $ixpinfo['vlans']   = $this->getListInfraVlans();
        $ixpinfo['switch'] = $this->getSwitchInfo( 'null' );
        $ixpinfo['switch_ports'] = $this->getListSwitchPorts();
        $ixpinfo['core_bundles'] = $this->getCoreBundles();
        return $ixpinfo;
    }    
    /**
     * Collate the IXP's switch information for the JSON schema export
     *
     * @return array
     */
   /**
     * Collate the IXP's switch information for the JSON schema export
     *
     * @param string            $version
     * @param Infrastructure    $infra
     *
     * @return array
     */
    private function getSwitchInfo( string $version ): array
    {
        $data = [];
	foreach( Infrastructure::all() as $infra ) {
        foreach( $infra->switchers as $switch ) {
            if( !$switch->active ) {
                continue;
            }

            $switchentry = [];
            $switchentry['id']      = $switch->id;
            $switchentry['name']    = $switch->name;
            $switchentry['colo']    = $switch->cabinet->location->name;
            $switchentry['city']    = $switch->cabinet->location->city ?? config( 'identity.location.city'    );
            $switchentry['country'] = $switch->cabinet->location->country ?? config( 'identity.location.country' )
;

            if( $switch->cabinet->location->pdb_facility_id ) {
                $switchentry['pdb_facility_id'] = (int)$switch->cabinet->location->pdb_facility_id;
            }

            if( $version >= self::EUROIX_JSON_VERSION_0_7 ) {
                $switchentry['manufacturer'] = $switch->vendor->name;
                $switchentry['model']        = $switch->model;
            }

            if( $version >= self::EUROIX_JSON_VERSION_1_0 ) {
                $switchentry['software'] = trim( ( $switch->os ?? '' ) . ' ' . ( $switch->osVersion ?? '' ) );
	    }
	    $switchentry['asn']            = $switch->asn;
            $switchentry['loopback_ip']    = $switch->loopback_ip;
            $switchentry['loopback_name']  = $switch->loopback_name;
    

            $data[] = $switchentry;
	    }
	}

        return $data;
    }

   /**
     * Collate the IXP's member information for the JSON schema export
     *
     * @return array
    */
    /**
     * Collate the IXP's member information for the JSON schema export
     *
     * @param string $version The version to collate the detail for
     * @param bool $detailed
     * @param bool $tags
     *
     * @return array
     */
    private function getMemberInfo( string $version, bool $detailed, bool $tags ): array
    {
        $memberinfo = [];

        if( $version === self::EUROIX_JSON_VERSION_0_7 ) {
            $routeServerIPs = [];
            Router::where( 'type', Router::TYPE_ROUTE_SERVER )
                ->get()->map( function($item) use(&$routeServerIPs) {
                    $routeServerIPs[$item->vlan_id][] = $item->peering_ip;
                });

            $routeCollectorIPs = [];
            Router::select( [ 'vlan_id', 'peering_ip' ] )
                ->where( 'type', Router::TYPE_ROUTE_COLLECTOR )
                ->get()->map( function($item) use(&$routeCollectorIPs) {
                    $routeCollectorIPs[$item->vlan_id][] = $item->peering_ip;
                });
        }

        if( $version === self::EUROIX_JSON_VERSION_1_0 ) {
            $routeServersByIps = [];
            Router::where( 'type', Router::TYPE_ROUTE_SERVER )
                ->get()->map( function($item) use(&$routeServersByIps) {
                    $routeServersByIps[ $item->vlan_id ][ $item->peering_ip ] = $item;
                });

            $routeCollectorsByIps = [];
            Router::where( 'type', Router::TYPE_ROUTE_COLLECTOR )
                ->get()->map( function($item) use(&$routeCollectorsByIps) {
                    $routeCollectorsByIps[ $item->vlan_id ][ $item->peering_ip ] = $item;
                });
        }

	$customers =  Customer::getConnected( false, false, 'autsys' )->keyBy( 'id') ->all();
        usort($customers, function($a, $b) {
            $a = $a->autsys;
            $b = $b->autsys;
            return (($a == $b) ? 0 : ($a < $b)) ? -1 : 1;
	});
        $cnt = 0;
        foreach( $customers as $c ) {
            /** @var Customer  $c */
            $connlist = [];

	    $fanoutlist = array_map( // take a look at application/views/customer/overview-tabs/ports/port.phtml to understand
              function( $vi ) {
		$phys_ints = $vi->physicalInterfaces->all();
                if( empty( $phys_ints ) ) {
                  return NULL; //says unassigned in UI
                }
                else {
                  return [
                    'id'        => $vi->id,
                    'customer'  => $phys_ints[0]->relatedInterface()->virtualInterface->customer->abbreviatedName,
                    'is_lag'    => (count($phys_ints)>1)? true : false,
                    'n_ports'   => count($phys_ints),
                    'ports'     => array_map(
                      function( $pi ) {
                        return [
                          'switch'  => $pi->switchPort->switcher->name,
                          'speed'   => $pi->speed,
                          'media'   => ($pi->switchPort->switcher->mauSupported)? $pi->switchPort()->mauType() : 'N/A',
                          'duplex'  => ($pi->switchPort->switcher->mauSupported)? 'N/A' : $pi->duplex,
                          'port'    => $pi->switchPort->name,
                        ];
                      },
                      $phys_ints
                    ),
                    'vlan_ifs'  => array_map(
                      function( $vli ) {
                        $vlanid = $vli->vlan->id;
                        if($vli->vlan->private){
                          return "private vlan handling not implemented yet";
                        }
                        else {
                          return [
                            'name'  => $vli->vlan->name,
                            'id'    => $vlanid,
                          ];
                        }
                        return [];
                      },
                      $vi->vlanInterfaces->all()
                    ),
                  ];
                }
              },
              array_values( array_filter( $c->virtualInterfaces->all() , function( $vi ) { return ( $vi->type() == SwitchPort::TYPE_FANOUT ); } ) )
            );

            foreach( $c->virtualInterfaces->all() as $vi )
            {
                //aduitsis 2017-05-17: we want to know whether this vi is a fanout vi
                $is_fanout = ( $vi->type() == SwitchPort::TYPE_FANOUT );

                $iflist = [];
                $atLeastOnePiIsPeering   = false;
                $atLeastOnePiIsConnected = false;
                foreach( $vi->physicalInterfaces as $pi )
                {
                    // hack for LONAP as they do peering on reseller ports :-(
                    //aduitsis 2017-05-17: We are stopping excluding fanout interfaces
                    if( !$is_fanout && !$pi->switchport->typePeering() && !$pi->switchport->typeReseller() ) {
                        continue;
                    }

                    $atLeastOnePiIsPeering = true;

                    if( $pi->status == PhysicalInterface::STATUS_CONNECTED ) {
                        $iflist[] = array (
                            //'switch_id'	=> $pi->getSwitchPort()->getSwitcher()->getId(),
                            'switch_name'           => $pi->switchPort->switcher->name,
                            'speed'                 => $pi->speed,
                            #'if_id'	=> $pi->getId(),
                            'if_name'               => $pi->switchPort->ifName,
                            'if_descr'              => $pi->switchPort->ifAlias,
                            'if_notes'              => ($pi->notes!=null)?$pi->notes:"",
                            'is_reseller_uplink'    => $pi->switchPort->TypeReseller(),
                            'has_fanout_interface'  => $pi->fanoutPhysicalInterface()->exists()? true : false,
                            'fanout_interface'      => $pi->fanoutPhysicalInterface()->exists()?
			    [
				# stayed here
                                'if_name'           => $pi->fanoutPhysicalInterface->switchPort->ifName,
                                'switch_name'       => $pi->fanoutPhysicalInterface->switchPort->switcher->name,
                              ]
                              :
                              NULL,
                            'duplex'                => $pi->duplex,
			    'status'                => $pi->status(),
                            'status_code'           => $pi->status,
                            'auto_negotiation'      => $pi->autoneg,
                        );
                        $atLeastOnePiIsConnected = true;
                    }
                } // physical interfaces list

                // see if this would cause something to be skipped for GR-IX
                // ANSWER: 2017-05-12 aduitsis: This makes fanout interfaces to be skipped
                // aduitsis 2017-05-17: We add !$is_fanout check to include fanout interfaces
                if( !$is_fanout && ( !$atLeastOnePiIsPeering || !$atLeastOnePiIsConnected ) ) {
                    continue;
                }

                // MAC addresses added in 0.4
		$macaddrs = $vi->macAddresses()->get()->all();
	        # old version of IXPMv5
		#if( $macaddrs[0] )
		if( !empty($macaddrs) )
		{
                    $vlanentry['mac_address'] = implode( ":", str_split( $macaddrs[0]->mac, 2 ) );
		}
                $atLeastOneVlanNotPrivate = false;

                $vlanlist = [];
                foreach( $vi->vlanInterfaces as $vli )
                {
                    $vlanentry = [];

                    // TODO in GR-IX, we might skip vlans with this
                    if( $vli->vlan->private ) {
                        continue;
                    }

                    $atLeastOneVlanNotPrivate = true;

                    // what if there's more than one vli ?
                    $vlanentry['vlan_id'] = $vli->vlan->id;
                    $vlanentry['vlan_number'] = $vli->vlan->number;
                    $vlanentry['vlan_name'] = $vli->vlan->name;

                    $vlanentry['l2_addresses'] = array_map(
			    function( $l2 ) {
				    if (is_a($l2,"IXP\Models\Layer2Address"))
				   {
					    return($l2->macFormatted( ':' ));
				    }
				return "test" ;
		      },
		    
                      $vli->layer2addresses->all()
                    );


                    /* TODO: ascertain why GR-IX doesn't want these two keys ipv4/6
                    if ($vli->getIpv4enabled()) {
                        $vlanentry['ipv4']['address'] = $vli->getIPv4Address()->getAddress();
                        $vlanentry['ipv4']['routeserver'] = $vli->getRsclient();
                        $vlanentry['ipv4']['max_prefix'] = $vi->getCustomer()->getMaxprefixes();
                        $vlanentry['ipv4']['as_macro'] = $vi->getCustomer()->resolveAsMacro( 4, "AS");
                    }
                    if ($vli->getIpv6enabled()) {
                        $vlanentry['ipv6']['address'] = $vli->getIPv6Address()->getAddress();
                        $vlanentry['ipv6']['routeserver'] = $vli->getRsclient();
                        $vlanentry['ipv6']['max_prefix'] = $vi->getCustomer()->getMaxprefixes();
                        $vlanentry['ipv6']['as_macro'] = $vi->getCustomer()->resolveAsMacro( 6, "AS" );
                    } */
                    $vlanlist[] = $vlanentry;
                }

                if( !$atLeastOneVlanNotPrivate ) {
                    continue;
                }

                $conn = [
                    'id'                => $vi->id,
                    'name'              => $vi->name,
                    'descr'             => $vi->description,
                    'trunk'             => $vi->trunk,
                    'type'              => $vi->type(),
                    'vlan_list'         => $vlanlist,
                    'phy_ports_count'   => count($iflist),
                    'if_list'           => $iflist,
                    'lag'               => $vi->lag_framing,
                    'fast_lacp'         => $vi->fastlacp,
                    //'channel_group'     => $vi->getChannelgroup(),
                    'bundle_name'       => $vi->bundleName(),
                    'mac_addresses'     => array_map( function( $mac ) {
                        return $mac->macColonsFormatted();
                      },
                      $vi->macAddresses->all()
                    ),
                    'is_fanout'         => $is_fanout,
                ];
                //TODO maybe these could be improved with PHP7?
                $conn_switches = array_unique(array_reduce($iflist,
                                                           function($carry, $item) {
                                                               $carry[] = $item['switch_name'];
                                                               return $carry;
                                                           },
                                                           array()));
                $conn_speeds = array_unique(array_reduce($iflist,
                                                           function($carry, $item) {
                                                               $carry[] = $item['speed'];
                                                               return $carry;
                                                           },
                                                           array()));
                $conn_statuses = array_unique(array_reduce($iflist,
                                                           function($carry, $item) {
                                                               $carry[] = $item['status'];
                                                               return $carry;
                                                           },
                                                           array()));
                $conn_status_codes = array_unique(array_reduce($iflist,
                                                           function($carry, $item) {
                                                               $carry[] = $item['status_code'];
                                                               return $carry;
                                                           },
                                                           array()));
                $conn_duplexes = array_unique(array_reduce($iflist,
                                                           function($carry, $item) {
                                                               $carry[] = $item['duplex'];
                                                               return $carry;
                                                           },
                                                           array()));
                $conn_auto_negotiations = array_unique(array_reduce($iflist,
                                                           function($carry, $item) {
                                                               $carry[] = $item['auto_negotiation'];
                                                               return $carry;
                                                           },
                                                           array()));

                if ( ($conn['name'] == '') && (count($iflist) != 1) ) {
                    $conn['if_name'] = 'ERROR_MULTIPLE_INTERFACES_BUT_NO_LAG_NAME';
                }
                else {
                    if ($conn['name'] == '') {
                        /* SIGNLE INTEFACE */
                        $conn['if_name'] = $iflist[0]['if_name'];
                        $conn['if_descr'] = $iflist[0]['if_descr'];
                        $conn['if_notes'] = $iflist[0]['if_notes'];
                        $conn['is_reseller_uplink'] = $iflist[0]['is_reseller_uplink'];
        	        }
                    else {
                        /* LAG INTEFACE */
                        #apolyr change:
                        #$conn['if_name'] = $conn['name'];
                        $conn['if_name'] = $conn['bundle_name'];
                        $conn['lag_members'] = $iflist;
                    }

                    if (count($conn_switches) == 1) {
                        $conn['switch_name'] = $conn_switches[0];
                    }
                    else {
                        $conn['switch_name'] = 'ERROR_MULTIPLE_SWITCHES_IN_A_SIGNLE_CONNECTION';
                    }

                    if (count($conn_speeds) == 1) {
                        $conn['speed'] = $conn_speeds[0];
                    }
                    else {
                        $conn['speed'] = 'ERROR_MULTIPLE_SPEEDS_IN_A_SIGNLE_CONNECTION';
                    }

                    $conn['status'] = (count($conn_statuses) == 1)? $conn_statuses[0] : 'ERROR_MULTIPLE_STATUSES';
                    $conn['status_code'] = (count($conn_status_codes) == 1)? $conn_status_codes[0] : 'ERROR_MULTIPLE_STATUS_CODES';
                    $conn['duplex'] = (count($conn_duplexes) == 1)? $conn_duplexes[0] : 'ERROR_MULTIPLE_DUPLEXES';
                    $conn['auto_negotiation'] = (count($conn_auto_negotiations) == 1)? $conn_auto_negotiations[0] : 'ERROR_MULTIPLE_AUTO_NEGOTIATION_VALUES';
                } // special handling of nameless conns

                $conn['ixp_id'] = 1;

                //$conn['state']       = 'active';
                //$conn['if_list']     = $iflist;

                $connlist[] = $conn;
            }// connection list
            $memberinfo[ $cnt ] = [
                'id'                  => $c->id,
                'asnum'               => $c->autsys,
                'name'                => $c->name,
                'shortname'           => $c->shortname,
                'connection_list'     => $connlist,
                'fanout_list'         => $fanoutlist,
                'is_reseller'         => $c->isReseller?True:False,
                'resold_customers'    => $c->isReseller?
                  array_map(
                    function( $i ) { return $i->shortname; },
                    $c->resoldCustomers->all()
                  )
                  :
		  NULL,
		  # Needed to be changed to depict resold_customer
                'is_resold_customer'  => $c->reseller?true:false,
                'reseller'            => $c->reseller?
                  $c->resellerObject->shortname : NULL,
                //'url'			     => $c->getCorpwww(),
                //'contact_email'		 => [ $c->getPeeringemail() ],
                //'contact_phone'		 => [ $c->getNocphone() ],
                //'peering_policy'	 => $c->getPeeringpolicy(),
                //'member_since'		 => $c->getDatejoin()->format( 'Y-m-d' ).'T00:00:00Z'
            ];

            if( filter_var( $c->nocwww, FILTER_VALIDATE_URL ) !== false ) {
                $memberinfo[ $cnt ]['peering_policy_url'] = $c->nocwww;
            }

            //if( $c->getNochours() && strlen( $c->getNochours() ) ) {
            //    $memberinfo[ $cnt ]['contact_hours'] = $c->getNochours();
            //}

            $memberinfo[$cnt][ 'type' ] = $this->xlateMemberType( $c->type );
	    $memberinfo[$cnt][ 'tags' ] = array();
	    foreach( $c->tags as $tag )
	    {
		    array_push($memberinfo[$cnt][ 'tags' ],$tag->tag);
	    }
            $cnt++;
        }

        return $memberinfo;
    }

    /**
     * Translate IXP Manager member types to JSON Export schema types
     *
     * @param int $ixpmType
     *
     * @return string
     */
    private function xlateMemberType( int $ixpmType ) : string
    {
        switch( $ixpmType ) {
            case Customer::TYPE_FULL:
                return 'peering';
            case Customer::TYPE_INTERNAL:
                return 'ixp';
            case Customer::TYPE_PROBONO:
                return 'probono';
            default:
                return 'other';
        }
    }

    private function getListSwitchPorts() {
        $data = [];
	$ptypes = SwitchPort::$TYPES;
	foreach( Infrastructure::all() as $infra ) {
            foreach( $infra->switchers as $switch ) {
                if( !$switch->active )
			continue;
                foreach( $switch->switchPorts as $switchport ) {
                    $data[$switch->name ][$switchport->name] = [
                      'type'             => $ptypes[ $switchport->type ],
                      'id'               => $switchport->id,
                      'name'             => $switchport->name,
                      'ifname'           => $switchport->ifName,
                      'ifalias'          => $switchport->ifAlias,
                      'ifhighspeed'      => $switchport->ifHighSpeed,
                      'ifmtu'            => $switchport->ifMtu,
                      'ifphysaddress'    => $switchport->ifPhysAddress,
                      'ifadminstatus'    => $switchport->ifAdminStatus,
                      'ifoperstatus'     => $switchport->ifOperStatus,
                      'iflastchange'     => $switchport->ifLastChange,
                      'lastsnmppoll'     => $switchport->lastSnmpPoll,
                      'ifindex'          => $switchport->ifIndex,
                      'active'           => ($switchport->active)?true:false,
                      'lagifindex'       => $switchport->LagIfIndex,
                    ];
                }
            }
      }
      return $data;
    }

    private function getCoreBundles() {
        function cidrToRange($cidr) {
        $range = array();
        $cidr = explode('/', $cidr);
        $range[0] = long2ip((ip2long($cidr[0])) & ((-1 << (32 - (int)$cidr[1]))));
        $range[1] = long2ip((ip2long($range[0])) + pow(2, (32 - (int)$cidr[1])) - 1);
        return $range;
        }

        $data = [];
        $ptypes = SwitchPort::$TYPES;
	    foreach( Infrastructure::all() as $infra ) {
            foreach( $infra->switchers as $switch ) {
                if( !$switch->active )
			continue;
		
		# global configuration per switch including asn, loopback_ip and the corresponding interface
        $data[$switch->name]['asn']            = $switch->asn;
        $data[$switch->name]['loopback_ip']    = $switch->loopback_ip;
        $data[$switch->name]['loopback_name']  = $switch->loopback_name;
		
		# virtual interface list
		$if_list=[];
		$if_speed=[];
		foreach( $switch->switchPorts as $sp ) {
			# If the physical port is core
			if( $sp->physicalInterface && $sp->physicalInterface->coreinterface ) {
				# create ipv4_address field and type
				$ipv4_address=Null;
				$type=Null;
				$active=False;
				# if the switch is from side a
				if  ( $sp->physicalInterface->coreinterface->corelinksidea && $sp->physicalInterface->coreinterface->corelinksidea->corebundle->enabled  )
				{
				        # if the port is L3-LAG we need to assign an IP address otherwise it is null
					if ($sp->physicalInterface->coreinterface->corelinksidea->corebundle->typeL3Lag())
					{
						# if the switch is from side a we assign the first IP of the given IP range
						$ipv4_address =cidrToRange($sp->physicalInterface->coreinterface->corelinksidea->corebundle->ipv4_subnet)[0];
					}
					$active=True;
					$type =$sp->physicalInterface->coreinterface->corelinksidea->corebundle->typeText();
					# get the directly connected switch
					$directly_connected_sw=$sp->physicalInterface->coreinterface->corelinksidea->coreInterfaceSideB->physicalInterface->SwitchPort->switcher->name;
                                        #$lag_descr =$sp->physicalInterface->coreinterface->corelinksidea->corebundle->description;
				}
				# if the switch is from side b
				elseif ($sp->physicalInterface->coreinterface->corelinksideb && $sp->physicalInterface->coreinterface->corelinksideb->corebundle->enabled)
				{

				        # if the port is L3-LAG we need to assign an IP address otherwise it is null
					if ($sp->physicalInterface->coreinterface->corelinksideb->corebundle->typeL3Lag())
					{       
				                # if the switch is from side b we assign the second IP of the given IP range
						$ipv4_address =cidrToRange($sp->physicalInterface->coreinterface->corelinksideb->corebundle->ipv4_subnet)[1];
					}
					$active=True;
					$type =$sp->physicalInterface->coreinterface->corelinksideb->corebundle->typeText();
					# get the directly connected switch
					$directly_connected_sw=$sp->physicalInterface->coreinterface->corelinksideb->coreInterfaceSideA->physicalInterface->SwitchPort->switcher->name;
                                        #$lag_descr =$sp->physicalInterface->coreinterface->corelinksideb->corebundle->description;
					
				}
				#we made a convention virtualinterface name + channel group
				$vifname=$sp->physicalInterface->virtualInterface->name.$sp->physicalInterface->virtualInterface->channelgroup;
				#$vifspeed=$sp->physicalInterface->virtualInterface->speed();
				#$if_speed[$vifname][]=$sp->physicalInterface->virtualInterface->speed();
				if (isset($ifspeed[$vifname]))
				{
				    	array_push($if_speed[$vifname][],$sp->ifHighSpeed);
				}
				else
				{	
					$if_speed[$vifname][]=$sp->ifHighSpeed;
				}
				
				#if core bundle is not active we do not include the lag interface
				if ($active)
					{
						$if_list[$vifname][]=[
							"name" =>$sp->name,
					                "directly_connected_switch" => $directly_connected_sw,
							# ATTENTION WE DO NOT KNOW THIS IFALIAS IF IT IS DEFINED IN IXPM OR RETRIEVED BY THE DEVICE
							#"if_descr"=>$sp->ifAlias,
						];
						$data[$switch->name]['lag_interfaces'][$vifname] = [
					"interfaces"	            => $if_list[$vifname],
					"ipv4_address"              => $ipv4_address,
					"type"                      => $type,
					"speed"                     => array_sum($if_speed[$vifname]),
					# if_descr = core_bundle_descr + 
					#"if_descr" =>$lag_descr." ".count($if_list[$vifname])."x".$physical_inteface_bw." ".$vifname
				];
					}

				}
			}	
				
            }
      }
      return $data;
    }
    private function getListInfraVlans() {
        $data = [];
	foreach( Infrastructure::all() as $infra ) {
            $infraname = $infra->name;
            foreach( $infra->vlans as $vlan ) {
	            $vdata = [
                'tag'         => $vlan->number,
                'name'        => $vlan->name,
                'configname'  => $vlan->config_name,
                'notes'       => $vlan->notes,
              ];
	            $data[$infraname][] = $vdata;
	          }
	      }
        return $data;
    }
}
