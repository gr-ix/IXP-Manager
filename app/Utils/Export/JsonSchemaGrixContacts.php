<?php namespace IXP\Utils\Export;

/*
 * This file is managed by puppet, DO NOT EDIT BY HAND!
 */

use OSS_Array;

/*
 * Copyright (C) 2009-2016 Internet Neutral Exchange Association Company Limited By Guarantee.
 * All Rights Reserved.
 *
 * This file is part of IXP Manager.
 *
 * IXP Manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version v2.0 of the License.
 *
 * IXP Manager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License v2.0
 * along with IXP Manager.  If not, see:
 *
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

use stdClass;

use IXP\Exceptions\Utils\ExportException;

use IXP\Models\{
    Customer,
    Infrastructure,
    NetworkInfo,
    Router
};
/**
 * JSON Schema Exporter
 *
 * Usage:
 *
 *     $jexport = new \IXP\Utils\Export\JsonSchema;
 *     $json_schema = $jexport->get( \IXP\Utils\Export\JsonSchema::EUROIX_JSON_LATEST );
 *
 * @see        https://github.com/euro-ix/json-schemas
 * @author     Nick Hilliard <nick@foobar.org>
 * @author     Barry O'Donovan <barry@opensolutions.ie>
 * @author     Zenon Mousmoulas <zmousm@noc.grnet.gr>
 * @author     Athanasios Douitsis <aduitsis@noc.ntua.gr>
 * @copyright  Copyright (C) 2009-2016 Internet Neutral Exchange Association Company Limited By Guarantee
 * @license    http://www.gnu.org/licenses/gpl-2.0.html GNU GPL V2.0
 */
class JsonSchemaGrixContacts
{
    // Supported versions:
    // ended 201705: const EUROIX_JSON_VERSION_0_3 = "0.3";
    // ended 201705: const EUROIX_JSON_VERSION_0_4 = "0.4";
    // ended 201705: const EUROIX_JSON_VERSION_0_5 = "0.5";
    public const EUROIX_JSON_VERSION_0_6 = "0.6";
    public const EUROIX_JSON_VERSION_0_7 = "0.7";
    public const EUROIX_JSON_VERSION_1_0 = "1.0";
    // adding a new version? update sanitiseVersion() below also!

    public const EUROIX_JSON_LATEST = self::EUROIX_JSON_VERSION_1_0;

    public const EUROIX_JSON_VERSIONS = [
        self::EUROIX_JSON_VERSION_0_6,
        self::EUROIX_JSON_VERSION_0_7,
        self::EUROIX_JSON_VERSION_1_0,
    ];

    /**
     * Get the JSON schema (for a given version or for the latest version)
     *
     * @param string|null   $version    The version to get (or, if null / not present then the latest)
     * @param bool          $asArray    Do not convert to JSON but rather return the PHP array
     * @param bool          $detailed   Create the very detailed version (usually for logged in users)
     * @param bool          $tags       Include customer tags
     * @return string|array
     * @throws
     */

    public function get( string $version = null, $asArray = true, $detailed = true, $tags = false )
    {

        if( $version === null ) {
            $version = self::EUROIX_JSON_LATEST;
        } else {
            $version = $this->sanitiseVersion( $version );
        }

        $output = [
            'version' => $version,
            'generator' => 'IXP Manager v' . APPLICATION_VERSION,
        ];
        // normalise times to UTC for exports
        date_default_timezone_set('UTC');
        $output['timestamp'] = now()->toIso8601ZuluString();
        $output['member_list'] = $this->getMemberInfo( $version, $detailed, $tags );

        if( $asArray ) {
            return $output;
        }
	return json_encode( $output , JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE )."\n";
    }
    /**
     * Ensure a given version exists or default to latest
     *
     * @param string $version Version string to sanitise
     *
     * @return string Sanitised version
     */
    //returns a line by line list of NOCs' emails
    // takes infra s argument, all infras if null
    public function getNocs( $infra = null, $field = 'noc_email' ) {

      $version= null;  
      if( $version === null ) {
            $version = self::EUROIX_JSON_LATEST;
      } else {
            $version = $this->sanitiseVersion( $version );
      }
      global $search_field;
      global $search_infra;
      $search_field = $field;
      $search_infra = $infra;
      #dd($this->getMemberInfo( 'null',true,true ));
      return join("\n",array_map(
        function($m) {
          global $search_field;
          global $search_infra;
          return $m[$search_field];
        },
        array_filter( $this->getMemberInfo( 'null',true,true ),
          function($member) {
            global $search_field;
            global $search_infra;
            return !empty($member[$search_field]) && ( !$search_infra || array_key_exists($search_infra,$member['infras']) );
          }
        )
      ));
    }

    // returns a line-by-line list of emails,names of all the contacts
    // in a usable form to be fed directly to sympa
    public function getSympa( $cat = 'dummy' , $infra = null )
    {
      $version = null;  
      if( $version === null ) {
            $version = self::EUROIX_JSON_LATEST;
      } else {
            $version = $this->sanitiseVersion( $version );
      }
      global $search_categ;
      global $search_infra;
      $search_categ = is_array($cat) ? $cat : explode(',',$cat);
      $search_infra = $infra;
      return join("\n",
        array_reduce(
          array_map(
            function( $m ) {
              return $m['contacts'];
            },
            array_values( array_filter( $this->getMemberInfo( $version,true,false ), function($member) { global $search_infra; return !empty($member['contacts']) && ( !$search_infra || array_key_exists($search_infra,$member['infras']) ); } ) )
          ),
          function( $carry, $item ) {
            foreach( $item as $i ) {
              global $search_categ;
              if( $i['email'] != null && $i['email'] != '' && array_intersect( $search_categ, $i['groups']) ) {
                $str = $i['email'].' '.$i['name'];
                array_push( $carry, $str );
              }
            };
            return $carry;
          },
          []
        )
      );
    }
    
    public function sanitiseVersion( string $version ): string
    {
        if( in_array( $version, self::EUROIX_JSON_VERSIONS ) ) {
            return $version;
        }

        return self::EUROIX_JSON_LATEST;
    }

    /**
     * Collate the IXP specific information for the JSON schema export
     *
     * @param string $version The version to collate the detail for
     *
     * @return array
     *
     * @throws
     */

    private function getIXPInfo( string $version ): array
    {
        $ixpinfo = [];

        foreach( Infrastructure::all() as $infra ) {
            $i = [];
            $i['shortname'] = $infra->name;
            $i['name'] = config('identity.legalname');
            $i['country'] = $infra->country ?? config('identity.location.country');
            $i['url'] = config('identity.corporate_url');

            if( $infra->peeringdb_ix_id ) {
                $i[ 'peeringdb_id' ] = (int)$infra->peeringdb_ix_id;
            }

            if( $infra->ixf_ix_id ) {
                $i[ 'ixf_id' ] = (int)$infra->ixf_ix_id;
            } else if( $version >= self::EUROIX_JSON_VERSION_0_7 ) {

                // The IX-F ID is officially required for >= v0.7 of the schema.
                // This shouldn't prevent the IX_F exporter from working though if someone wishes to pull the
                // information regardless of that being set.
                //
                // Two options for this:

                // first pass an ixfid for **every** infrastructure that does not have one
                // e.g. http://ixp-inex.ldev/api/v4/member-export/ixf/1.0?ixfid_1=30&ixfid_2=31&ixfid_3=30
                if( request('ixfid_' . $infra->id, false ) ) {
                    $i[ 'ixf_id' ] = (int)request( 'ixfid_' . $infra->id );
                }

                // second, just ignore it and set it to zero:
                // http://ixp-inex.ldev/api/v4/member-export/ixf/1.0?ignore_missing_ixfid=1
                else if( request('ignore_missing_ixfid', false ) ) {
                    $i[ 'ixf_id' ] = 0;
                }

                // by default, we will throw an exception:
                else {
                    throw new ExportException( "The IX-F ID is a required parameter for IX-F Export Schema >=v0.7.
 Set this in IXP Manager in the 'Infrastructures' management page." );
                }
            }

            $i['ixp_id'] = $infra->id;    // referenced in member's connections section

            $i['support_email'] = config('identity.support_email');
            $i['support_phone'] = config('identity.support_phone');
            $i['support_contact_hours'] = config('identity.support_hours');

            // $infra['stats_api'] = FIXME;
            $i['emergency_email'] = config('identity.support_email');
            $i['emergency_phone'] = config('identity.support_phone');
            $i['emergency_contact_hours'] = config('identity.support_hours');
            $i['billing_contact_hours'] = config('identity.billing_hours');
            $i['billing_email'] = config('identity.billing_email');
            $i['billing_phone'] = config('identity.billing_phone');

            $i['peering_policy_list'] = array_values( Customer::$PEERING_POLICIES);


            $result = NetworkInfo::leftJoin( 'vlan', 'vlan.id', 'networkinfo.vlanid' )
                ->where( 'vlan.infrastructureid', $infra->id )
                ->get()->toArray();

            $vlanentry = [];
            foreach( $result as $ni )
            {
                $id = $ni['id'];
                $vlanentry[$id]['id']                                   = $ni['id'];
                $vlanentry[$id]['name']                                 = $ni['name'];
                $vlanentry[$id][ 'ipv'.$ni['protocol'] ]['prefix']      = $ni[ 'network' ];
                $vlanentry[$id][ 'ipv'.$ni['protocol'] ]['mask_length'] = $ni[ 'masklen' ];
            }

            $data = [];
            foreach( array_keys($vlanentry) as $id ) {
                $data[] = $vlanentry[$id];
            }

            $i['vlan'] = $data;

            if( $version >= self::EUROIX_JSON_VERSION_0_7 ) {
                if( !config( 'ixp_fe.frontend.disabled.lg' ) ) {
                    foreach( $i[ 'vlan' ] as $idx => $vlan ) {
                        if( isset( $i[ 'vlan' ][ $idx ][ 'ipv4' ] ) ) {
                            $i[ 'vlan' ][ $idx ][ 'ipv4' ][ 'looking_glass_urls' ][] = url( '/lg' );
                        }
                        if( isset( $i[ 'vlan' ][ $idx ][ 'ipv6' ] ) ) {
                            $i[ 'vlan' ][ $idx ][ 'ipv6' ][ 'looking_glass_urls' ][] = url( '/lg' );
                        }
                    }
                }
            }

            $i['switch'] = $this->getSwitchInfo( $version, $infra );

            $ixpinfo[] = $i;
        }

        return $ixpinfo;
    }
   /**
     * Collate the IXP's switch information for the JSON schema export
     *
     * @param string            $version
     * @param Infrastructure    $infra
     *
     * @return array
     */
    private function getSwitchInfo( string $version, Infrastructure $infra ): array
    {
        $data = [];

        foreach( $infra->switchers as $switch ) {
            if( !$switch->active ) {
                continue;
            }

            $switchentry = [];
            $switchentry['id']      = $switch->id;
            $switchentry['name']    = $switch->name;
            $switchentry['colo']    = $switch->cabinet->location->name;
            $switchentry['city']    = $switch->cabinet->location->city ?? config( 'identity.location.city'    );
            $switchentry['country'] = $switch->cabinet->location->country ?? config( 'identity.location.country' )
;

            if( $switch->cabinet->location->pdb_facility_id ) {
                $switchentry['pdb_facility_id'] = (int)$switch->cabinet->location->pdb_facility_id;
            }

            if( $version >= self::EUROIX_JSON_VERSION_0_7 ) {
                $switchentry['manufacturer'] = $switch->vendor->name;
                $switchentry['model']        = $switch->model;
            }

            if( $version >= self::EUROIX_JSON_VERSION_1_0 ) {
                $switchentry['software'] = trim( ( $switch->os ?? '' ) . ' ' . ( $switch->osVersion ?? '' ) );
            }

            $data[] = $switchentry;
        }

        return $data;
    }

   /**
     * Collate the IXP's member information for the JSON schema export
     *
     * @return array
    */
    /**
     * Collate the IXP's member information for the JSON schema export
     *
     * @param string $version The version to collate the detail for
     * @param bool $detailed
     * @param bool $tags
     *
     * @return array
     */
    private function getMemberInfo( string $version, bool $detailed, bool $tags ): array
    {
        $memberinfo = [];

        if( $version === self::EUROIX_JSON_VERSION_0_7 ) {
            $routeServerIPs = [];
            Router::where( 'type', Router::TYPE_ROUTE_SERVER )
                ->get()->map( function($item) use(&$routeServerIPs) {
                    $routeServerIPs[$item->vlan_id][] = $item->peering_ip;
                });

            $routeCollectorIPs = [];
            Router::select( [ 'vlan_id', 'peering_ip' ] )
                ->where( 'type', Router::TYPE_ROUTE_COLLECTOR )
                ->get()->map( function($item) use(&$routeCollectorIPs) {
                    $routeCollectorIPs[$item->vlan_id][] = $item->peering_ip;
                });
        }

        if( $version === self::EUROIX_JSON_VERSION_1_0 ) {
            $routeServersByIps = [];
            Router::where( 'type', Router::TYPE_ROUTE_SERVER )
                ->get()->map( function($item) use(&$routeServersByIps) {
                    $routeServersByIps[ $item->vlan_id ][ $item->peering_ip ] = $item;
                });

            $routeCollectorsByIps = [];
            Router::where( 'type', Router::TYPE_ROUTE_COLLECTOR )
                ->get()->map( function($item) use(&$routeCollectorsByIps) {
                    $routeCollectorsByIps[ $item->vlan_id ][ $item->peering_ip ] = $item;
                });
        }
        
        $customers =  Customer::getConnected( false, false, 'autsys' )->keyBy( 'id' );

        $cnt = 0;
        foreach( $customers as $c ) {
            /** @var Customer  $c */
	    $d = $c->companyRegisteredDetail;
            $memberinfo[ $cnt ] = [
                'id'             => $c->id,
                'connected'      => true,
                'current_active' => $c->currentActive(),
                'asnum'          => $c->autsys,
                'name'           => $c->name,
                'shortname'      => $c->shortname,
		'url'            => $c->corpwww,
                'registered_name'     => $d->registeredName,
                'company_number'      => $d->companyNumber,
                'jurisdiction'        => $d->jurisdiction,
                'address1'            => $d->address1,
                'address2'            => $d->address2,
                'address3'            => $d->address3,
                'towncity'            => $d->townCity,
                'postcode'            => $d->postcode,
                'country'             => $d->country,
                'contact_email' => [ $c->peeringemail ],
		'contact_phone' => [ $c->nocphone ],
		'noc_email'     =>  $c->nocemail,
                'member_since'   => $c->datejoin->format( 'Y-m-d' ).'T00:00:00Z',
                'peering_policy' => $c->peeringpolicy,
            ];

	    if( $detailed ) {
		$contacts=array();
		foreach($c->contacts as $customer_contacts)
		{
			array_push($contacts,$customer_contacts);
		}
                $memberinfo[ $cnt ] = array_merge( $memberinfo[ $cnt ], [
		    'contacts'      => array_map(
                    function( $contact ) {
                    return [
                      'groups'              => array_map(
                        function( $group ) {
                          return $group->name;
                        },
                        $contact->contactGroupsAll()->get()->all()
                      ),
                      'name'                => $contact->name,
                      'email'               => $contact->email,
                      'phone'               => $contact->phone,
                      'mobile'              => $contact->mobile,
                      'facility_access'     => $contact->facilityaccess,
                      'may_authorize'       => $contact->mayauthorize,
                      'position'            => $contact->position,
                    ];
                  },
                  $contacts
	  )
	]);

            if( filter_var($c->nocwww, FILTER_VALIDATE_URL) !== false ) {
                    $memberinfo[ $cnt ][ 'peering_policy_url' ] = $c->nocwww;
            }

            if( $c->nochours && strlen( $c->nochours ) ) {
                    $memberinfo[ $cnt ][ 'contact_hours' ] = $c->nochours;
            }
	    $memberinfo[$cnt][ 'type' ] = $this->xlateMemberType( $c->type );
            $b = $c->companyBillingDetail;
            $memberinfo[$cnt][ 'billing' ] = [
              'billing_contact'           => $b->billingContactName,
              'invoice_email'             => $b->invoiceEmail,
              'billing_address_1'         => $b->billingAddress1,
              'billing_address_2'         => $b->billingAddress2,
              'billing_address_3'         => $b->billingAddress3,
              'billing_town_city'         => $b->billingTownCity,
              'billing_postcode'          => $b->billingPostcode,
              'billing_country'           => $b->billingCountry,
              'billing_email'             => $b->billingEmail,
              'billing_telephone'         => $b->billingTelephone,
              'vat_number'                => $b->vatNumber,
              'var_rate'                  => $b->vatRate,
              'puchase_order_required'    => ($b->purchaseOrderRequired?true:false),
              'invoice_method'            => $b->invoiceMethod,
              'billing_frequency'         => $b->billingFrequency,
      ];
                $memberinfo[$cnt]['infras']=[]; //always make sure that the array exists
		foreach( $c->virtualInterfaces()->get() as $vi ) 
		{
			foreach( $vi->vlanInterfaces()->get() as $vli ) 
			{
				foreach ($vli->vlan()->get() as $vlinfra)
				{
					foreach ($vlinfra->infrastructure()->get() as $vlinfraname)
					{
					$infra = $vlinfraname->shortname;
        	          		$memberinfo[$cnt]['infras'][ $infra ] = true;
					}
				}

            		}
            	}

            }

            if( $tags ) {
                $memberinfo[ $cnt ][ 'ixp_manager' ][ 'tags' ] = [];
                foreach( $c->tags as $tag ) {
                    if( !$tag->internal_only || $detailed ) {
                        $memberinfo[ $cnt ][ 'ixp_manager' ][ 'tags' ][ $tag->tag ] = $tag->display_as;
                    }
                }
            }


            $cnt++;
        }

        return $memberinfo;
    }

    /**
     * Translate IXP Manager member types to JSON Export schema types
     *
     * @param int $ixpmType
     *
     * @return string
     */
    private function xlateMemberType( int $ixpmType ) : string
    {
        switch( $ixpmType ) {
            case Customer::TYPE_FULL:
                return 'peering';
            case Customer::TYPE_INTERNAL:
                return 'ixp';
            case Customer::TYPE_PROBONO:
                return 'peering';
            default:
                return 'other';
        }
    }

    private function getListSwitchPorts() {
        $data = [];

        $ptypes = \Entities\SwitchPort::$TYPES;

        $ixp = d2r( 'IXP' )->getDefault();
        foreach( $ixp->getInfrastructures() as $infra ) {
            foreach( $infra->getSwitchers() as $switch ) {
                if( !$switch->getActive() )
                  continue;
                foreach( $switch->getPorts() as $switchport ) {
                    $data[$switch->getName()][$switchport->getName()] = [
                      'type'             => $ptypes[ $switchport->getType() ],
                      'id'               => $switchport->getId(),
                      'name'             => $switchport->getName(),
                      'ifname'           => $switchport->getIfName(),
                      'ifalias'          => $switchport->getIfAlias(),
                      'ifhighspeed'      => $switchport->getIfHighSpeed(),
                      'ifmtu'            => $switchport->getIfMtu(),
                      'ifphysaddress'    => $switchport->getIfPhysAddress(),
                      'ifadminstatus'    => $switchport->getIfAdminStatus(),
                      'ifoperstatus'     => $switchport->getIfOperStatus(),
                      'iflastchange'     => $switchport->getIfLastChange(),
                      'lastsnmppoll'     => $switchport->getLastSnmpPoll(),
                      'ifindex'          => $switchport->getIfIndex(),
                      'active'           => $switchport->getActive(),
                      'lagifindex'       => $switchport->getLagIfIndex(),
                    ];
                }
            }
      }
      return $data;
    }

    private function getListInfraVlans() {
        $data = [];

        $ixp = d2r( 'IXP' )->getDefault();
        foreach( $ixp->getInfrastructures() as $infra ) {
            $infraname = $infra->getName();
            foreach( $infra->getVlans() as $vlan ) {
	            $vdata = [
                'tag'         => $vlan->getNumber(),
                'name'        => $vlan->getName(),
                'configname'  => $vlan->getConfigName(),
                'notes'       => $vlan->getNotes(),
              ];
	            $data[$infraname][] = $vdata;
	          }
	      }
        return $data;
    }

}
