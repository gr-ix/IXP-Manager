<?php

namespace IXP\Http\Controllers;

use App, Auth, Cache, Countries, D2EM, DateTime, Former, Mail, Redirect;

use IXP\Events\Customer\BillingDetailsChanged as CustomerBillingDetailsChangedEvent;

use IXP\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\{
    RedirectResponse,
    Request
};

use Illuminate\View\View;

use IXP\Models\{
    Aggregators\CustomerAggregator,
    Aggregators\RsPrefixAggregator,
    CompanyBillingDetail,
    CompanyRegisteredDetail,
    Customer,
    CustomerNote,
    CustomerTag,
    IrrdbConfig,
    NetworkInfo,
    Router,
    Vlan
};

use Entities\{
    BgpSession              as BgpSessionEntity,
    CompanyBillingDetail    as CompanyBillingDetailEntity,
    CompanyRegisteredDetail as CompanyRegisteredDetailEntity,
    Customer                as CustomerEntity,
    CustomerNote            as CustomerNoteEntity,
    CustomerTag             as CustomerTagEntity,
    IRRDBConfig             as IRRDBConfigEntity,
    IXP                     as IXPEntity,
    NetworkInfo             as NetworkInfoEntity,
    RSPrefix                as RSPrefixEntity,
    User                    as UserEntity,
    Vlan                    as VlanEntity
};


use IXP\Http\Requests\Customer\{
    Store                   as CustomerRequest,
    BillingInformation      as BillingInformationRequest,
    WelcomeEmail            as WelcomeEmailRequest
};

use IXP\Models\{
    Cabinet,
#    Customer,
    CustomerEquipment
};


class MailingController extends Controller
{
    public function list( Request $r, $start_date = null, $stop_date = null, $issue = null, $outlook = true ): View {

        $fmt = "F Y";
        $month = date( "n" );
        $year = date( "Y" );

        if( ! $start_date ) {
          if ( $month <=7 and $month>1) {
            $start_date = "January 1, $year";
          }
          else {
            $start_date = "July 1, $year";
          }
        }
        if( ! $stop_date ) {
          if ( $month <=7 and $month>1 ) {
            $stop_date = "June 30, $year";
          }
          else {
            $stop_date = "December 31, $year";
          }
        }
        if( ! $issue ) {
          if ( $month <=7 and $month>1 ) {
            $issue = "June 30, $year";
          }
          else {
            $issue = "December 31, $year";
          }
        }
        # dd( $start_date, $stop_date, $issue, $outlook );

	# IXPManager6 version
	$state = true;
	$type = null;
	$showCurrentOnly = true;
	#$tid = false;
	if( ( $state = $r->state ) !== null ) {
            if( isset( Customer::$CUST_STATUS_TEXT[ $state ] ) ) {
                $r->session()->put( "cust-list-state", $state );
            } else {
                $r->session()->remove( "cust-list-state" );
            }
        } else if( $r->session()->exists( "cust-list-state" ) ) {
            $state = $r->session()->get( "cust-list-state" );
        }

        if( ( $type = $r->type ) !== null ) {
            if( isset( Customer::$CUST_TYPES_TEXT[ $type ] ) ) {
                $r->session()->put( "cust-list-type", $type );
            } else {
                $r->session()->remove( "cust-list-type" );
            }
        } else if( $r->session()->exists( "cust-list-type" ) ) {
            $type = $r->session()->get( "cust-list-type" );
        }

        if( ( $showCurrentOnly = $r->input( 'current-only' ) ) !== null ) {
            $r->session()->put( "cust-list-current-only", $showCurrentOnly );
        } else if( $r->session()->exists( "cust-list-current-only" ) ) {
            $showCurrentOnly = $r->session()->get( "cust-list-current-only" );
        } else {
            $showCurrentOnly = false;
        }

        $tags = CustomerTag::all()->keyBy( 'id' )->toArray();

        if( $r->tag  !== null ) {
            if(  isset( $tags[ $r->tag ] ) ) {
                $tid = $tags[ $r->tag ][ 'id' ];
                $r->session()->put( "cust-list-tag", $tid );
            } else {
                $r->session()->remove( "cust-list-tag" );
                $tid = false;
            }
        } else if( $r->session()->exists( "cust-list-tag" ) ) {
            $tid = $r->session()->get( "cust-list-tag" );
        } else {
            $tid = false;
        }
	# IXPManager5
	#$custs = D2EM::getRepository( CustomerEntity::class )->getAllForFeList( true, 1, null, false );
	$custs =  Customer::getConnected( false, false);
        $data = [];

	foreach($custs as $cust){
	  
          $data[ $cust->id ] = MailingController::construct_record_from_customer( $cust, $start_date, $stop_date, $issue, $outlook );
        }
        //dd( $custs2 );

        return view( 'mailing/list' )->with([
            'custs'                 => $custs,
            'data'                  => $data,
        ]);
    }


    private static function construct_record_from_customer( $customer, $start_date, $stop_date, $issue, $outlook ) {
          $days = 30;
          $separator = ($outlook)? ';' : ',';
          $r = [];
          $to_arr = [];
          $r['shortname'] = $customer->shortname;
          $r['subject'] = "GR-IX services & cost analysis (period $start_date-$stop_date)";
          $r['subject2'] = "GR-IX e-invoice (period $start_date-$stop_date)";
          $r['subject7'] = "Greek Internet Exchange :: Member information update";
          $name = $customer->name;
          $r['name'] = $name;
          $billing = $customer->companyBillingDetail;
          $reg = $customer->companyRegisteredDetail;
          $company_address = '    Company Name: '.$customer->name."\n    ".$billing->vatNumber.($billing->vatNumber?"\n":'').'    '.trim(preg_replace('/\s+/', ' ', ''.$reg->address1.($reg->address2? ",".$reg->address2:'').($reg->address3? ",".$reg->address3:'').", ".$reg->TownCity." ".$reg->postcode.", ".$reg->country));


          if ( $customer->companyBillingDetail->invoiceMethod == 'POST' ) {
            $billing_header = "A hardcopy invoice will be send to the following address and contact person:";
            $billing = '    '.$customer->companyBillingDetail->billingContactName."\n    ".$customer->companyBillingDetail->billingEmail."\n    ".$customer->companyBillingDetail->billingTelephone."\n    ".$customer->companyBillingDetail->billingAddress1."\n    ".$customer->companyBillingDetail->billingAddress2.($billing->billingAddress2?"\n    ":'').$customer->companyBillingDetail->billingPostcode." ".$customer->companyBillingDetail->billingCountry;
          }
          else if ( $customer->companyBillingDetail->invoiceMethod == 'EMAIL' ) {
            $billing_header = "An electronic invoice will be send to the following email address:";
            $billing = $customer->companyBillingDetail->invoiceEmail;
            array_push( $to_arr , $customer->companyBillingDetail->invoiceEmail );
          }
          else {
            $billing_header = "???";
            $billing = "UNKNOWN";
          }

          $admin_persons          = join("\n", array_unique(  MailingController::getContacts($customer, array('Admin Person') ) ) );
          $admin_group     	  = join("\n", array_unique(  MailingController::getContacts($customer, array('Admin Group') ) ) );
          $billing_group          = join("\n", array_unique(  MailingController::getContacts($customer, array('Billing Group') ) ) );
          $billing_persons        = join("\n", array_unique(  MailingController::getContacts($customer, array('Billing Person') ) ) );
          $legal_representative   = join("\n", array_unique(  MailingController::getContacts($customer, array('Legal Representative') ) ) );
          $technical_persons      = join("\n", array_unique(  MailingController::getContacts($customer, array('Technical Person') ) ) );
          $technical_group        = join("\n", array_unique(  MailingController::getContacts($customer, array('Technical Group') ) ) );
          $users = $customer->users()->get();
          $users_info=array();

	  foreach($users as $user) {
		  $user_info="username: ".$user->username.", email: ".$user->email;
		  array_push($users_info,$user_info);
	  }
	  $users_info = join("\n",$users_info);

          $noc_emails = array( $customer->nocemail );

          $r['to']  = join($separator, array_unique( array_merge( $to_arr, MailingController::getMails($customer) ) ) );      
          $r['cc'] = 'support@gr-ix.gr';
          $r['cc7'] = 'helpdesk@gr-ix.gr';
          $r['to2'] = join($separator, array_unique( array_merge( $to_arr, MailingController::getMails($customer) ) ) );      
          $r['to3'] = join($separator, array_unique( array_merge( $to_arr, MailingController::getMails($customer) ) ) ); # admin, billing, invoice
          $r['to4'] = join($separator, array_unique( MailingController::getMails($customer, array('admin','admin-list','Admin Group','Admin Person','Technical Group','Technical Person','tech','tech-list') ) ) ); # admin, tech
          $r['to5'] = join($separator, array_unique( MailingController::getMails($customer, array('admin','admin-list','Admin Group','Admin Person') ) ) ); # admin
          $r['to6'] = join($separator, array_unique( array_merge( $noc_emails,  MailingController::getMails($customer, array('Technical Group','Technical Person','tech','tech-list') ) ) ) ); # tech, noc
          $r['to7'] = join($separator, array_unique( MailingController::getMails($customer, array('Admin Group','Admin Person') ) ) ); # admin
          $r['body'] = "Dear $name,

Please find attached an analysis of the provided GR-IX services and the associated costs for the period $start_date-$stop_date. Given that no changes will occur up to the end of the billing period, an invoice will be issued on $issue.

***Please review the service analysis, the associated costs and the invoicing information. Report errors or changes within the next 5 business days.***

Invoice data:
-----------------
$company_address

Invoice recipient:
-----------------------
$billing_header

$billing

Electronic copy:
-------------------------
Also, an electronic copy of the invoice will be emailed to the recipients of this email (administrative & billing contacts).

More information on this contract and services:
-----------------------------------------------------------------------
The following persons within your company are defined as administrative contacts for your contract with GR-IX and they can provide further information about that contract and the services that you receive:
$admin_persons

GR-IX contacts:
---------------------------
- For inquiries with regard to the services, dates, pricelist etc: Mr John Korakis, jhn@noc.grnet.gr, (+30) 2107474249
- For inquiries with regard to invoicing and payments: Mrs Anna Loukakou, loukakou@grnet.gr, (+30) 2107474256


Kind regards,
";
          $r['body2'] = "Dear $name,

Please find attached the e-Invoice for the provided GR-IX services and the associated costs for the period $start_date-$stop_date. For your convenience we also attach an analysis of the provided services for that period. 

More information on this contract and services:
-----------------------------------------------------------------------
The following persons within your company are defined as administrative contacts for your contract with GR-IX and they can provide further information about that contract and the services that you receive:
$admin_persons

GR-IX contacts:
---------------------------
- For inquiries with regard to the services, dates, pricelist etc: Mr John Korakis, jhn@noc.grnet.gr, (+30) 2107474249
- For inquiries with regard to invoicing and payments: Mrs Anna Loukakou, loukakou@grnet.gr, (+30) 2107474256

Kind regards,
";
          $r['body7'] = "Dear $name,

Please, take some time to review and, if needed, update your information in the GR-IX portal (https://portal.gr-ix.gr/), especially your contacts' and users' lists.

Your current contacts:
-----------------------------------------------------------------------
Admin Person:
$admin_persons
Admin Group:
$admin_group


-----------------------------------------------------------------------
Billing Person:
$billing_persons        
Billing Group:
$billing_group

-----------------------------------------------------------------------
Legal Representative:
$legal_representative

-----------------------------------------------------------------------
Technical Person:
$technical_persons
Technical Group:
$technical_group

-----------------------------------------------------------------------
Your current users:
$users_info
Thank you very much,

The GR-IX team
";
          $r['text_type'] = $customer->type();
          return $r;
}


// copied from JsonSchemGrixContacts
    public static function getMails( $customer, $cat = array('billing','billing-list','admin','admin-list','Admin Group','Admin Person','Billing Group','Billing Person') )
    {
      global $search_categ;
      $search_categ = is_array($cat) ? $cat : array($cat);
      #$contacts = $customer->contacts;#->get();#->contactGroupsAll()->get();
      #dd($contacts);
      $contacts=array();
      #if ($customer->id==52) {
      foreach($customer->contacts as $c)
      {
       	      array_push($contacts,$c);
      }
      $arr =
      array_map(
        function( $i ) { return $i['email']; },
        array_filter(
          array_map(
              function( $contact ) {
                return [
                  'groups'=>array_map(
                    function( $group ) {
                      return $group->name;
                    },
                    $contact->contactGroupsAll()->get()->all()
                  ),
                  'email' => ( $contact->email ) ? $contact->email:'',
                ];
              },
              $contacts
          ),
          function( $i ) {
            global $search_categ;
            if( $i['email'] != null && $i['email'] != '' && array_intersect( $search_categ, $i['groups']) ) {
              return true;
            }
            else {
              return false;
            }
          }
        )
	);
      //if ($customer->id==29)
      #dd( $arr );
      return $arr;
    }

// copied from JsonSchemGrixContacts
    public static function getContacts( $customer, $cat = array('billing','billing-list','admin','admin-list','Admin Group','Admin Person','Billing Group','Billing Person') )
    {
      global $search_categ;
      $search_categ = is_array($cat) ? $cat : array($cat);
      $contacts=array();
      foreach($customer->contacts as $c)
      {
       	      array_push($contacts,$c);
      }
      $arr =
      array_map(
        function( $i ) { return '- '.$i['name'].', '.$i['email'].', '.$i['phone']; },
        array_filter(
          array_map(
              function( $contact ) {
                return [
                  'groups'=>array_map(
                    function( $group ) {
                      return $group->name;
                    },
                    $contact->contactGroupsAll()->get()->all()
                  ),
                  'email' => ( $contact->email ) ? $contact->email:'',
                  'name'  => ( $contact->name  ) ? $contact->name:'',
                  'phone' => ( $contact->phone ) ? $contact->phone:'(no phone)',
                ];
              },
              $contacts
          ),
          function( $i ) {
            global $search_categ;
            if( $i['email'] != null && $i['email'] != '' && array_intersect( $search_categ, $i['groups']) ) {
              return true;
            }
            else {
              return false;
            }
          }
        )
      );
      //dd( $arr );
      return $arr;
    }

    public function listAction() {
        $fmt = "F Y";
        $month = date( "n" );
        $year = date( "Y" );
        if ( $month <=7 and $month>1 ) {
          $start_date = "January 1, $year";
          $stop_date = "June 30, $year";
          $issue = "June 30, $year";
        }
        else {
          $start_date = "July 1, $year";
          $stop_date = "December 31, $year";
          $issue = "December 31, $year";
        }
        $days = $this->getParam('days',30);
        $start_date = $this->getParam('start', $start_date );
        $stop_date = $this->getParam('stop', $stop_date );
        $issue = $this->getParam('issue', $issue );
        $arr = $this->listGetData( null );

        $outlook = $this->getParam('outlook',true);
        $separator = ($outlook)? ';' : ',';



        foreach($arr as &$r){
          //dd($r);
          $to_arr = array();
          $r['subject'] = "GR-IX services & cost analysis (period $start_date-$stop_date)";
          $r['subject2'] = "GR-IX e-invoice (period $start_date-$stop_date)";
          $r['subject7'] = "GR-IX Contacts Update";
          $name = $r['name'];
          $customer = d2r( 'Customer' )->find($r['id']);
          $billing = $customer->companyBillingDetail;
          $reg = $customer->getRegistrationDetails();
          $company_address = '    Company Name: '.$reg->getRegisteredName()."\n    ".$billing->getVatNumber().($billing->getVatNumber()?"\n":'').'    '.trim(preg_replace('/\s+/', ' ', ''.$reg->getAddress1().($reg->getAddress2()? ",".$reg->getAddress2():'').($reg->getAddress3()? ",".$reg->getAddress3():'').", ".$reg->getTownCity()." ".$reg->getPostcode().", ".$reg->getCountry()));


          if ( $customer->companyBillingDetail->getInvoiceMethod() == 'POST' ) {
            $billing_header = "A hardcopy invoice will be send to the following address and contact person:";
            $billing = '    '.$customer->companyBillingDetail->billingContactName."\n    ".$customer->companyBillingDetail->billingEmail."\n    ".$customer->companyBillingDetail->billingTelephone."\n    ".$customer->companyBillingDetail->billingAddress1."\n    ".$customer->companyBillingDetail->billingAddress2.($billing->billingAddress2?"\n    ":'').$customer->companyBillingDetail->billingPostcode." ".$customer->companyBillingDetail->billingCountry;
          }
          else if ( $customer->companyBillingDetail->invoiceMethod == 'EMAIL' ) {
            $billing_header = "An electronic invoice will be send to the following email address:";
            $billing = $customer->companyBillingDetail->invoiceEmail;
            array_push( $to_arr , $customer->companyBillingDetail->invoiceEmail );
          }
          else {
            $billing_header = "???";
            $billing = "UNKNOWN";
          }

          $admin_persons = join("\n", array_unique( $this->getContacts($customer, array('Admin Person') ) ) );

          // dd( $this->getMails($customer) ); 
          $r['to']  = join($separator, array_unique( array_merge( $to_arr, $this->getMails($customer) ) ) );      
          $r['cc'] = 'support@gr-ix.gr';
          $r['to2'] = join($separator, array_unique( array_merge( $to_arr, $this->getMails($customer) ) ) );      
          $r['to3'] = join($separator, array_unique( array_merge( $to_arr, $this->getMails($customer) ) ) ); # admin, billing, invoice
          $r['to4'] = join($separator, array_unique( $this->getMails($customer, array('admin','admin-list','Admin Group','Admin Person','Technical Group','Technical Person','tech','tech-list') ) ) ); # admin, tech
          $r['to5'] = join($separator, array_unique( $this->getMails($customer, array('admin','admin-list','Admin Group','Admin Person') ) ) ); # admin
          $r['to6'] = join($separator, array_unique( array_merge( $noc_emails,  $this->getMails($customer, array('Technical Group','Technical Person','tech','tech-list') ) ) ) ); # tech, noc
          $r['to7'] = join($separator, array_unique( $this->getMails($customer, array('admin') ) ) ); # admin
          $r['body'] = "Dear $name,

Please find attached an analysis of the provided GR-IX services and the associated costs for the period $start_date-$stop_date. Given that no changes will occur up to the end of the billing period, an invoice will be issued on $issue.

***Please review the service analysis, the associated costs and the invoicing information. Report errors or changes within the next 5 business days.***

Invoice data:
-----------------
$company_address

Invoice recipient:
-----------------------
$billing_header

$billing

Electronic copy:
-------------------------
Also, an electronic copy of the invoice will be emailed to the recipients of this email (administrative & billing contacts).

More information on this contract and services:
-----------------------------------------------------------------------
The following persons within your company are defined as administrative contacts for your contract with GR-IX and they can provide further information about that contract and the services that you receive:
$admin_persons

GR-IX contacts:
---------------------------
- For inquiries with regard to the services, dates, pricelist etc: Mr John Korakis, jhn@noc.grnet.gr, (+30) 2107474249
- For inquiries with regard to invoicing and payments: Mrs Anna Loukakou, loukakou@grnet.gr, (+30) 2107474256


Kind regards,
";
          $r['body2'] = "Dear $name,

Please find attached the e-Invoice for the provided GR-IX services and the associated costs for the period $start_date-$stop_date. For your convenience we also attach an analysis of the provided services for that period. 

More information on this contract and services:
-----------------------------------------------------------------------
The following persons within your company are defined as administrative contacts for your contract with GR-IX and they can provide further information about that contract and the services that you receive:
$admin_persons

GR-IX contacts:
---------------------------
- For inquiries with regard to the services, dates, pricelist etc: Mr John Korakis, jhn@noc.grnet.gr, (+30) 2107474249
- For inquiries with regard to invoicing and payments: Mrs Anna Loukakou, loukakou@grnet.gr, (+30) 2107474256

Kind regards,
";

          $r['body7'] = "Dear $name,

You may find below the contacts that you have provided for GR-IX. Please update them accordingly.

-----------------------------------------------------------------------
The following persons within your company are defined as administrative contacts for your contract with GR-IX and they can provide further information about that contract and the services that you receive:
$admin_persons

GR-IX contacts:
---------------------------
- For inquiries with regard to the services, dates, pricelist etc: Mr John Korakis, jhn@noc.grnet.gr, (+30) 2107474249
- For inquiries with regard to invoicing and payments: Mrs Anna Loukakou, loukakou@grnet.gr, (+30) 2107474256

Kind regards,
";

          $r['text_type'] = $customer->type();
        }
        //dd( $arr );


        $this->view->data = $arr;
    }



}
