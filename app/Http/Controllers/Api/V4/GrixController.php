<?php

declare(strict_types=1);
namespace IXP\Http\Controllers\Api\V4;

/*
 * This is a test comment
 * Copyright (C) 2009 - 2019 Internet Neutral Exchange Association Company Limited By Guarantee.
 * All Rights Reserved.
 *
 * This file is part of IXP Manager.
 *
 * IXP Manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version v2.0 of the License.
 *
 * IXP Manager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License v2.0
 * along with IXP Manager.  If not, see:
 *
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

use Auth;

use Illuminate\Http\{
    Request,
    JsonResponse,
    Response
};

use IXP\Utils\Export\JsonSchema as JsonSchemaExporter;
use IXP\Utils\Export\JsonSchemaGrix as JsonSchemaGrix;
use IXP\Utils\Export\JsonSchemaGrixContacts as JsonSchemaGrixContacts;
use IXP\Models\Infrastructure;

/**
 * PublicController
 *
 * @author     Barry O'Donovan <barry@islandbridgenetworks.ie>
 * @category   APIv4
 * @package    IXP\Http\Controllers\Api\V4
 * @copyright  Copyright (C) 2009 - 2019 Internet Neutral Exchange Association Company Limited By Guarantee
 * @license    http://www.gnu.org/licenses/gpl-2.0.html GNU GPL V2.0
 */
class GrixController extends Controller {

    /**
     * Simple test route for API authentication
     *
     * Documented at: http://docs.ixpmanager.org/features/api/
     *
     * @return Response
     */
    public function test(): Response {
        return response()->make( "API Test Function!\n\nAuthenticated: "
                . ( (Auth::check() ? 'Yes, as: ' . Auth::user()->getUsername() : 'No' )) . "\n\n", 200 )
            ->header( 'Content-Type', 'text/plain; charset=utf-8' );
    }

    /**
     * Simple ping route for basic public information.
     *
     * @return JsonResponse
     */
    public function ping(): JsonResponse {

        /** @var Infrastructure $i */
	    $infras = [];
	    $infras = Infrastructure::select( [ 'i.name', 'i.shortname', 'i.ixf_ix_id', 'i.peeringdb_ix_id' ] ) ->from( 'infrastructure AS i' )->get()->toArray();


        return response()->json([
            'software' => "IXP Manager",
            'version'  => APPLICATION_VERSION,
            'verdate'  => APPLICATION_VERDATE,
            'url'      => url(''),
            'ixf-export' => config( 'ixp_api.json_export_schema.public' ),
            'infrastructures' => $infras,
            'identity' => [
                'sitename'  => config( 'identity.sitename' ),
                'legalname' => config( 'identity.legalname' ),
                'orgname'   => config( 'identity.orgname' ),
                'corp_url'  => config( 'identity.corporate_url' ),
                'city'      => config( 'identity.location.city' ),
                'country'   => config( 'identity.location.country' ),
            ],
        ], 200, [], JSON_PRETTY_PRINT );
    }

    public function list() {

        if( !Auth::check() && !config( 'ixp_api.json_export_schema.public', false ) ) {
            abort(401, 'Public access not permitted' );
        }

        $grix = new JsonSchemaGrix;
	$resp = $grix->get();
	return response()->json( $resp, 200, [], JSON_PRETTY_PRINT )->header( "Access-Control-Allow-Origin", "*" );
	# IXPManagerv5 required the following
        #return response()->json( $resp, 200, [], JSON_PRETTY_PRINT )->header( "Access-Control-Allow-Origin", "*" );
    }

    public function contacts() {

        if( !Auth::check() && !config( 'ixp_api.json_export_schema.public', false ) ) {
            abort(401, 'Public access not permitted' );
        }
        $grix = new JsonSchemaGrixContacts;
	$resp = $grix->get();
	return response()->json( $grix->get(), 200, [], JSON_PRETTY_PRINT )->header( "Access-Control-Allow-Origin", "*" );
    }

    public function sympa( string $category, string $infra = null ) {

        if( !Auth::check() && !config( 'ixp_api.json_export_schema.public', false ) ) {
            abort(401, 'Public access not permitted' );
        }

        $grix = new JsonSchemaGrixContacts;
        $resp = $grix->getSympa($category,$infra);
        return response( $resp, 200 )->header("Content-Type",'text/plain; charset="UTF-8"')->header( "Access-Control-Allow-Origin", "*" );
    }

    public function nocs( string $infra = null ) {

        if( !Auth::check() && !config( 'ixp_api.json_export_schema.public', false ) ) {
            abort(401, 'Public access not permitted' );
        }

        $grix = new JsonSchemaGrixContacts;
        $resp = $grix->getNocs($infra);
        return response( $resp, 200 )->header("Content-Type",'text/plain; charset="UTF-8"')->header( "Access-Control-Allow-Origin", "*" );
    }

}
