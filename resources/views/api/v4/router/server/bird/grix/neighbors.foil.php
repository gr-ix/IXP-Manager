<?php foreach( $t->ints as $int ):
  // do not set up a session to ourselves!
  if( $int['autsys'] == $t->router->asn() ):
    continue;
  endif;
?>
### Customer ID: <?= $int['cid'] ?> - Full name: <?= $int['cname'] ?> - Shortname: <?= $int['cshortname'] ?>

### AS number: <?= $int['autsys'] ?> - IPv4/6 address: <?= $int['address'] ?> - ID in peering VLAN: <?= $int['vliid'] ?> (<?= sprintf("%04d",$int['vliid']) ?>)
### Full location name: <?= $int['location_name'] ?> - Location shortname: <?= $int['location_shortname'] ?> - Location tag: <?= $int['location_tag'] ?>

### Peering macro: <?= $int['peeringmacro'] ?> - Maximum # of prefixes: <?= $int['maxprefixes'] ?> - BGP MD5 secret: <?= $int['bgpmd5secret'] ?>


table t_<?= $int['cshortname'] ?><?= $int['vliid'] ?>;

filter peer_<?= $int['cshortname'] ?><?= $int['vliid'] ?>_to_master
prefix set <?= $int['cshortname'] ?>_prefixes;
int set <?= $int['cshortname'] ?>_ASNs;
{
        # Basic prefix validation. Do not propagate funny prefixes. 
        # Check first AS in AS-PATH, esp to prevent route servers peering to each other
        if !(avoid_martians()) then reject;
        if (bgp_path.first != <?= $int['autsys'] ?> ) then reject;


<?php
  if($int['irrdbfilter']) : ?>
        # Check AS-PATH against the appropriate IRRDB macro
        <?= $int['cshortname'] ?>_ASNs = [ <?php
    $arr = $int['irrdbfilter_asns'];
    $arr2= [];
    $i=0;
    while( $item = array_shift( $arr ) ):
      $i++;
      array_push($arr2,$item);
      if (($i % 10 == 0) or (!$arr)) :
        echo join(', ',$arr2);
        if($arr): ?>
,                 # <?= $int['cshortname'] ?>-<?= $int['vliid'] ?> - AS<?= $int['autsys'] ?> - <?= $int['address'] ?>
<?php   echo "\n                  ";
        $arr2 = [];
        endif;
      endif;
    endwhile;
?> ];

        if !(bgp_path.last ~ <?= $int['cshortname'] ?>_ASNs ) then reject "REJECTING ",net.ip,"/",net.len," received by <?= $int['cshortname'] ?>-<?= $int['vliid'] ?>, AS<?= $int['autsys'] ?>: Origin AS",bgp_path.last," not in AS-SET: <?= $int['peeringmacro'] ?> ";

<?php
    if( count( $int['irrdbfilter_prefixes'] ) > 0 ): ?>
        # Check each prefix against the route objects originating from the ASNs in the appropriate IRRDB macro
        <?= $int['cshortname'] ?>_prefixes = [ <?= join(',',$int['irrdbfilter_prefixes']) ?> ];

        if ! (net ~ <?= $int['cshortname'] ?>_prefixes ) then reject "REJECTING ",net.ip,"/",net.len," received by <?= $int['cshortname'] ?>-<?= $int['vliid'] ?>, AS<?= $int['autsys'] ?>: No route object with origin AS",bgp_path.last," for this prefix";
<?php
    else: ?>
        # Deny everything because the IRRDB returned nothing
        reject;
<?php
    endif; ?>
<?php
  else: ?>
        # This ASN was configured not to use IRRDB filtering
<?php
  endif; ?>

        # Prepend 1,2,3 times, if (RSasn,65001), (RSasn,65002), (RSasn,65003) have been set
        if (RSasn,65501) ~ bgp_community then bgp_path.prepend(RSasn);
        if (RSasn,65502) ~ bgp_community then { bgp_path.prepend(RSasn); bgp_path.prepend(RSasn); }
        if (RSasn,65503) ~ bgp_community then { bgp_path.prepend(RSasn); bgp_path.prepend(RSasn); bgp_path.prepend(RSasn); }

        # Mark prefixies with "site" community
<?php
  $conv = [
    'EIE' => 65101,
    'LH'  => 65102,
    'TIS' => 65103,
    'SNC' => 65111,
  ];
?>
        bgp_community.add ((RSasn,<?=$conv[$int['location_shortname']]?>));

        # Done filtering & manipulation. Accept!
        accept;
}

protocol pipe p_<?= $int['cshortname'] ?><?= $int['vliid'] ?> {
        description "Pipe for <?= $int['cshortname'] ?><?= $int['vliid'] ?> (AS<?= $int['autsys'] ?> - <?= $int['address'] ?>)";
        table master;
        mode transparent;
        peer table t_<?= $int['cshortname'] ?><?= $int['vliid'] ?>;
        import filter peer_<?= $int['cshortname'] ?><?= $int['vliid'] ?>_to_master;
        export where master_to_as(<?= $int['autsys'] ?>,"<?= $int['location_shortname'] ?>",0);
}

protocol bgp b_<?= $int['cshortname'] ?><?= $int['vliid'] ?> from grix_rs_client {
        description "BGP for <?= $int['cshortname'] ?><?= $int['vliid'] ?> (AS<?= $int['autsys'] ?> - <?= $int['address'] ?>)";
        neighbor <?= $int['address'] ?> as <?= $int['autsys'] ?>;
        import limit <?= $int['maxprefixes'] ?>;
        table t_<?= $int['cshortname'] ?><?= $int['vliid'] ?>;
        bfd on; # This should be configured for specific peers in the future.
<?php
  if($int['bgpmd5secret']):?>
        password "<?=$int['bgpmd5secret']?>";
<?php
  endif; ?>
}

<?php
endforeach; ?>
