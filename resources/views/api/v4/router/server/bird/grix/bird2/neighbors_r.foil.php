<?php foreach( $t->ints as $int ):
  // do not set up a session to ourselves!
  if( $int['autsys'] == $t->router->asn ):
    continue;
  endif;
?>
### Customer ID: <?= $int['cid'] ?> - Full name: <?= $int['cname'] ?> - Shortname: <?= $int['cshortname'] ?>

### AS number: <?= $int['autsys'] ?> - IPv4/6 address: <?= $int['address'] ?> - ID in peering VLAN: <?= $int['vliid'] ?> (<?= sprintf("%04d",$int['vliid']) ?>)
### Full location name: <?= $int['location_name'] ?> - Location shortname: <?= $int['location_shortname'] ?> - Location tag: <?= $int['location_tag'] ?>

### Peering macro: <?= $int['peeringmacro'] ?> - Maximum # of prefixes: <?= $int['maxprefixes'] ?> - BGP MD5 secret: <?= $int['bgpmd5secret'] ?>


ipv<?= $t->router->protocol ?> table t_<?= $int['cshortname'] ?><?= $int['vliid'] ?>;

filter peer_<?= $int['cshortname'] ?><?= $int['vliid'] ?>_to_master
prefix set <?= $int['cshortname'] ?>_prefixes;
int set <?= $int['cshortname'] ?>_ASNs;
{
        # Basic prefix validation. Do not propagate funny prefixes. 
        # Check first AS in AS-PATH, esp to prevent route servers peering to each other
        if !(avoid_martians()) then reject;
        if (bgp_path.first != <?= $int['autsys'] ?> ) then reject;

        if bgp_large_community ~ [ IXP_LC_FILTERED_RPKI_INVALID ] then {
            print "REJECTING RPKI_INVALID ",net.ip,"/",net.len," received by <?= $int['cshortname']?><?= $int['vliid'] ?>, AS<?= $int['autsys'] ?>: origin AS",bgp_path.last;
            reject;
        }


<?php
  if($int['irrdbfilter']) : ?>
        # these should fire always insted of the "other" rejects below
        if bgp_large_community ~ [ IXP_LC_FILTERED_IRRDB_PREFIX_FILTERED ]  then reject "REJECTING IRRDB_PREFIX_FILTERED ",net.ip,"/",net.len," received by <?= $int['cshortname'] ?>-<?= $int['vliid'] ?>, AS<?= $int['autsys'] ?>: Origin AS",bgp_path.last;
        if bgp_large_community ~ [ IXP_LC_FILTERED_IRRDB_ORIGIN_AS_FILTERED ]  then reject "REJECTING IRRDB_ORIGIN_AS_FILTERED ",net.ip,"/",net.len," received by <?= $int['cshortname'] ?>-<?= $int['vliid'] ?>, AS<?= $int['autsys'] ?>: No route object with origin AS",bgp_path.last," for this prefix";

<?php
  else: ?>
        # This ASN was configured not to use IRRDB filtering
<?php
  endif; ?>

        # Prepend 1,2,3 times, if (RSasn,65001), (RSasn,65002), (RSasn,65003) have been set
        if (RSasn,65501) ~ bgp_community then bgp_path.prepend(RSasn);
        if (RSasn,65502) ~ bgp_community then { bgp_path.prepend(RSasn); bgp_path.prepend(RSasn); }
        if (RSasn,65503) ~ bgp_community then { bgp_path.prepend(RSasn); bgp_path.prepend(RSasn); bgp_path.prepend(RSasn); }

        # Mark prefixies with "site" community
<?php
  $conv = [
    'EIE' => 65101,
    'LH'  => 65102,
    'TIS' => 65103,
    'SNC' => 65111,
  ];
?>
        bgp_community.add ((RSasn,<?=$conv[$int['location_shortname']]?>));

        # Done filtering & manipulation. Accept!
        accept;
}


filter filter_bgp_<?= $int['cshortname'] ?><?= $int['vliid'] ?> 
prefix set <?= $int['cshortname'] ?>_prefixes;
int set <?= $int['cshortname'] ?>_ASNs;
{

        ## Prevent BGP NEXT_HOP Hijacking
        if !( from = bgp_next_hop ) then
            reject "REJECTING ",net.ip,"/",net.len,": BGP neighbor address [", from, "] does not match next hop address [", bgp_next_hop, "]";

        ## RPKI checks
        if( roa_check( t_roa, net, bgp_path.last_nonaggregated ) = ROA_INVALID ) then {
                print "Tagging invalid ROA ", net, " for ASN ", bgp_path.last;
                bgp_large_community.add( IXP_LC_FILTERED_RPKI_INVALID );
                bgp_large_community.add( IXP_LC_INFO_RPKI_INVALID );
        }
        else if( roa_check( t_roa, net, bgp_path.last_nonaggregated ) = ROA_VALID ) then {
                print "Valid ROA ", net, " from ",bgp_next_hop," for ASN ", bgp_path.last;
                bgp_large_community.add( IXP_LC_INFO_RPKI_VALID );
        }
        # not needed -- else if( roa_check( t_roa, net, bgp_path.last_nonaggregated ) = ROA_UNKNOWN ) then {
        else {
                print "Unknown ROA ", net, " from ",bgp_next_hop," for ASN ", bgp_path.last;
                bgp_large_community.add( IXP_LC_INFO_RPKI_UNKNOWN );
        }

<?php
  if($int['irrdbfilter']) : ?>
        <?= $int['cshortname'] ?>_ASNs = [ <?php
    $arr = \IXP\Models\Aggregators\IrrdbAggregator::asnsForRouterConfiguration( $int[ 'cid' ], $t->router->protocol);
    $arr2= [];
    $i=0;
    while( $item = array_shift( $arr ) ):
      $i++;
      array_push($arr2,$item);
      if (($i % 10 == 0) or (!$arr)) :
        echo join(', ',$arr2);
        if($arr): ?>
,                 # <?= $int['cshortname'] ?>-<?= $int['vliid'] ?> - AS<?= $int['autsys'] ?> - <?= $int['address'] ?>
<?php   echo "\n                  ";
        $arr2 = [];
        endif;
      endif;
    endwhile;
?> ];

        if !(bgp_path.last ~ <?= $int['cshortname'] ?>_ASNs ) then {
            bgp_large_community.add( IXP_LC_FILTERED_IRRDB_ORIGIN_AS_FILTERED );
        }

<?php
    $prefixes= \IXP\Models\Aggregators\IrrdbAggregator::prefixesForRouterConfiguration( $int[ 'cid' ], $t->router->protocol );	
    if( count( $prefixes ) > 0 ): ?>
        # Check each prefix against the route objects originating from the ASNs in the appropriate IRRDB macro
        <?= $int['cshortname'] ?>_prefixes = [ <?= join(',',$prefixes) ?> ];

        if ! (net ~ <?= $int['cshortname'] ?>_prefixes ) then {
            bgp_large_community.add( IXP_LC_FILTERED_IRRDB_PREFIX_FILTERED );
        }
        else {
            bgp_large_community.add( IXP_LC_INFO_IRRDB_VALID );
        }

<?php
    else: ?>
        # Deny everything because the IRRDB returned nothing
        reject;
<?php
    endif; ?>
<?php
  else: ?>
        # This ASN was configured not to use IRRDB filtering
<?php
  endif; ?>

        accept;
}

protocol pipe {
        description "Pipe for <?= $int['cshortname'] ?><?= $int['vliid'] ?> (AS<?= $int['autsys'] ?> - <?= $int['address'] ?>)";
        table master;
        peer table t_<?= $int['cshortname'] ?><?= $int['vliid'] ?>;
        #mode transparent;
        import filter peer_<?= $int['cshortname'] ?><?= $int['vliid'] ?>_to_master;
        export where master_to_as(<?= $int['autsys'] ?>,"<?= $int['location_shortname'] ?>",0);
}

protocol bgp b_<?= $int['cshortname'] ?><?= $int['vliid'] ?> from grix_rs_client {
        description "BGP for <?= $int['cshortname'] ?><?= $int['vliid'] ?> (AS<?= $int['autsys'] ?> - <?= $int['address'] ?>)";
        neighbor <?= $int['address'] ?> as <?= $int['autsys'] ?>;
        ipv<?= $t->router->protocol ?> {
            table t_<?= $int['cshortname'] ?><?= $int['vliid'] ?>;
            export all;
            import limit <?= $int['maxprefixes'] ?> action restart;
            import filter filter_bgp_<?= $int['cshortname'] ?><?= $int['vliid'] ?>;
        };
        ### bfd on; # This should be configured for specific peers in the future.
<?php
  if($int['bgpmd5secret']):?>
        password "<?=$int['bgpmd5secret']?>";
<?php
  endif; ?>
}

<?php
endforeach; ?>
