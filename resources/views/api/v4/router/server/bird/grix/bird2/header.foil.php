#################################################################################
#
# Bird Route Server configuration for VLAN: <?= $t->vlan->name ?> (Tag: <?= $t->vlan->number ?>)
#
# -------------------------------------------------------------------------------
# This configuration was generated automatically by IXP Manager
#      Generated: <?= date('Y-m-d H:i:s') . "\n" ?>
#
# Attention: Do not edit manually, it will be overwritten automatically.
#
#################################################################################

#################################################################################
# BASIC DOCUMENTATION
#
#
# 1. Communities
# --------------
#
# ** Advertisement Control: **
# RSasn,PeerAS (1<=PeerAS<65000) or route origin extended community ro,RSasn,PeerAS, PeerAS>65536): Do not advertise to PeerAS
# RSasn,0 : Inverse advertising policy (do not advertise to any peers, except from those defined with RSasn:PeerASn)
#
# ** Prepending (communities can be combined)**
# RSasn,65501: prepend one time
# RSasn,65502: prepend two times
# RSasn,65503: prepend three times
#
# ** MED: **
# RSasn,65000: Do not alter incoming MED for IX switching optimisation
#
# ** Site-Marking: **
# RSasn,65101: Prefix received from a peering at ATH01 (EIE) site
# RSasn,65102: Prefix received from a peering at ATH02 (LH) site
# RSasn,65103: Prefix received from a peering at ATH03 (TIS) site
# RSasn,65111: Prefix received from a peering at THESS01 (SNC) site
#
#
# 2. Route selection
# ------------------
# 1. Prefer route with the highest Local Preference attribute.
#	(no practical use, Local Pref is not used in our implementation)
# 2. Prefer route with the shortest AS path.
#	(in use)	
# 3. Prefer IGP origin over EGP and EGP origin over incomplete.
#	(in use)	
# 4. Prefer the lowest value of the Multiple Exit Discriminator.
#	(in use, BUT only MED from prefixes advertised by peerings of the SAME AS are compared (always-compare-med not enabled))	
# 5. Prefer routes received via eBGP over ones received via iBGP.
#	(no practical use, all peerings are eBGP)	
# 6. Prefer routes with lower internal distance to a boundary router.
#	(no practical use, all distances are the same)	
# 7. Prefer older routes
#	(this is not part of the route selection algorithm but it is a configurable option of the route server to avoid unstable toutes)
# 8. Prefer the route with the lowest value of router ID of the advertising router.
#	(in use)
#
#
#################################################################################


timeformat base     iso long;
timeformat log      iso long;
timeformat protocol iso long;
timeformat route    iso long;

log "/var/log/bird/bird-<?= $t->handle ?>.log" { warning, error, fatal, trace, remote };

# Turn on global debugging for events and states for all protocols
debug protocols { events, states };

define RSasn = <?= $t->router->asn ?>;
define RSaddress = <?= $t->router->peering_ip ?>;

define IXP_LC_FILTERED_PREFIX_LEN_TOO_LONG      = ( RSasn, 1101, 1  );
define IXP_LC_FILTERED_PREFIX_LEN_TOO_SHORT     = ( RSasn, 1101, 2  );
define IXP_LC_FILTERED_BOGON                    = ( RSasn, 1101, 3  );
define IXP_LC_FILTERED_BOGON_ASN                = ( RSasn, 1101, 4  );
define IXP_LC_FILTERED_AS_PATH_TOO_LONG         = ( RSasn, 1101, 5  );
define IXP_LC_FILTERED_AS_PATH_TOO_SHORT        = ( RSasn, 1101, 6  );
define IXP_LC_FILTERED_FIRST_AS_NOT_PEER_AS     = ( RSasn, 1101, 7  );
define IXP_LC_FILTERED_NEXT_HOP_NOT_PEER_IP     = ( RSasn, 1101, 8  );
define IXP_LC_FILTERED_IRRDB_PREFIX_FILTERED    = ( RSasn, 1101, 9  );
define IXP_LC_FILTERED_IRRDB_ORIGIN_AS_FILTERED = ( RSasn, 1101, 10 );
define IXP_LC_FILTERED_PREFIX_NOT_IN_ORIGIN_AS  = ( RSasn, 1101, 11 );
define IXP_LC_FILTERED_RPKI_UNKNOWN             = ( RSasn, 1101, 12 );
define IXP_LC_FILTERED_RPKI_INVALID             = ( RSasn, 1101, 13 );
define IXP_LC_FILTERED_TRANSIT_FREE_ASN         = ( RSasn, 1101, 14 );
define IXP_LC_FILTERED_TOO_MANY_COMMUNITIES     = ( RSasn, 1101, 15 );
########
define IXP_LC_INFO_RPKI_VALID                   = ( RSasn, 1000, 1  );
define IXP_LC_INFO_RPKI_UNKNOWN                 = ( RSasn, 1000, 2  );
define IXP_LC_INFO_RPKI_NOT_CHECKED             = ( RSasn, 1000, 3  );
define IXP_LC_INFO_RPKI_INVALID                 = ( RSasn, 1000, 4  );
define IXP_LC_INFO_IRRDB_INVALID                = ( RSasn, 1001, 0  );
define IXP_LC_INFO_IRRDB_VALID                  = ( RSasn, 1001, 1  );
define IXP_LC_INFO_IRRDB_NOT_CHECKED            = ( RSasn, 1001, 2  );
define IXP_LC_INFO_IRRDB_MORE_SPECIFIC          = ( RSasn, 1001, 3  );
define IXP_LC_INFO_IRRDB_FILTERED_LOOSE         = ( RSasn, 1001, 1000 );
define IXP_LC_INFO_IRRDB_FILTERED_STRICT        = ( RSasn, 1001, 1001 );
define IXP_LC_INFO_IRRDB_PREFIX_EMPTY           = ( RSasn, 1001, 1002 );
define IXP_LC_INFO_FROM_IXROUTESERVER           = ( RSasn, 1001, 1100 );
define IXP_LC_INFO_SAME_AS_NEXT_HOP             = ( RSasn, 1001, 1200 );


router id <?= $t->router->router_id ?>;
# listen bgp address <?= $t->router->peering_ip ?>;

# ignore interface up/down events
protocol device { }
protocol direct { }

roa<?= $t->router->protocol ?> table t_roa;

protocol rpki rpki1 {
        roa<?= $t->router->protocol ?> { table t_roa; };
        remote 62.217.85.149 port 8282;
}

protocol rpki rpki2 {
        roa<?= $t->router->protocol ?> { table t_roa; };
        remote 62.217.126.68 port 8282;
}

### Filter Martians Function
# This function excludes weird networks
#  rfc1918, class D, class E, too long and too short prefixes
function avoid_martians()
prefix set martians;
{
    <?php if( $t->router->protocol == 6 ): ?>

        martians = [
                ::/0,                   # Default (can be advertised as a route in BGP to peers if desired)
                ::/96,                  # IPv4-compatible IPv6 address <E2><80><93> deprecated by RFC4291
                ::/128,                 # Unspecified address
                ::1/128,                # Local host loopback address
                ::ffff:0.0.0.0/96+,     # IPv4-mapped addresses
                ::224.0.0.0/100+,       # Compatible address (IPv4 format)
                ::127.0.0.0/104+,       # Compatible address (IPv4 format)
                ::0.0.0.0/104+,         # Compatible address (IPv4 format)
                ::255.0.0.0/104+,       # Compatible address (IPv4 format)
                0000::/8+,              # Pool used for unspecified, loopback and embedded IPv4 addresses
                0200::/7+,              # OSI NSAP-mapped prefix set (RFC4548) <E2><80><93> deprecated by RFC4048
                3ffe::/16+,             # Former 6bone, now decommissioned
                2001:db8::/32+,         # Reserved by IANA for special purposes and documentation
                2002:e000::/20+,        # Invalid 6to4 packets (IPv4 multicast)
                2002:7f00::/24+,        # Invalid 6to4 packets (IPv4 loopback)
                2002:0000::/24+,        # Invalid 6to4 packets (IPv4 default)
                2002:ff00::/24+,        # Invalid 6to4 packets
                2002:0a00::/24+,        # Invalid 6to4 packets (IPv4 private 10.0.0.0/8 network)
                2002:ac10::/28+,        # Invalid 6to4 packets (IPv4 private 172.16.0.0/12 network)
                2002:c0a8::/32+,        # Invalid 6to4 packets (IPv4 private 192.168.0.0/16 network)
                fc00::/7+,              # Unicast Unique Local Addresses (ULA) <E2><80><93> RFC 4193
                fe80::/10+,             # Link-local Unicast
                fec0::/10+,             # Site-local Unicast <E2><80><93> deprecated by RFC 3879 (replaced by ULA)
                ff00::/8+               # Multicast
        ];

    <?php else: ?>

        martians = [
                10.0.0.0/8+,
		100.64.0.0/10+,
		127.0.0.0/8+,
                169.254.0.0/16+,
                172.16.0.0/12+,
                192.0.0.0/24+,
                192.0.2.0/24+,
                192.168.0.0/16+,
                198.18.0.0/15+,
                198.51.100.0/24+,
                203.0.113.0/24+,
                224.0.0.0/4+,
                240.0.0.0/4+,
                0.0.0.0/32-,
                0.0.0.0/0{28,32},
                0.0.0.0/0{0,7}
        ];

     <?php endif; ?>

        # Avoid RFC1918 and similar networks
        if net ~ martians then {
		print "REJECTING ",net.ip,"/",net.len," received by ",from,": Martian/too long/too short prefix";
		return false;
		}

        return true;
}

### Filtering between members Function
# Applied on the pipes between the master table and each members' table
# peeras: the AS of the peer towards which the prefixes are exported
# site: the GR-IX POP name where the peering is located
# dbg: set to 1 to print debug messages for this peer (check the log file)
function master_to_as(int peeras;string site;int dbg)
{

	# reject non-BGP routes
	if ! (source = RTS_BGP) then {
		if (dbg=1) then print "DEBUG ",peeras,": REJECT ",net.ip,"/",net.len," from ",from,", REASON: not a BGP prefix";
		return false;
	}

        if  ( ((RSasn,0) ~ bgp_community) || ((rt,RSasn,0) ~ bgp_ext_community) ) then {        # Default reject policy. Allow only (Rsasn,peeras) or (rt,Rsasn,peeras)
                if !( (peeras <= 65535 && ((RSasn,peeras) ~ bgp_community)) 
                      || ((rt,RSasn,peeras) ~ bgp_ext_community) 
                    ) then {
                        if (dbg=1) then print "DEBUG ",peeras,": REJECT ",net.ip,"/",net.len," from ",from,", REASON: Default reject and not implicitely allowed ",bgp_community," - ",bgp_ext_community;
                        return false;
                }
        } else  {                                     # Default accept policy. Deny (Rsasn,peeras) or (rt,Rsasn,peeras)
                if ( (peeras <= 65535 && ((RSasn,peeras) ~ bgp_community)) 
                     || ((rt,RSasn,peeras) ~ bgp_ext_community) 
                   ) then {
                        if (dbg=1) then print "DEBUG ",peeras,": REJECT ",net.ip,"/",net.len," from ",from,", REASON: Default announce, but explicitely forbidden ",bgp_community," - ",bgp_ext_community;
                        return false;
                }
        }

        if !((RSasn,65000) ~ bgp_community) then { # MED values are not used by the member; they can be used to optimised switching within the exchange
                bgp_med = 0;
                if ( site = "EIE" && (RSasn,65102) ~ bgp_community ) then bgp_med = 2;
                if ( site = "EIE" && (RSasn,65103) ~ bgp_community ) then bgp_med = 4;
                if ( site = "LH" && (RSasn,65101) ~ bgp_community ) then bgp_med = 2;
                if ( site = "LH" && (RSasn,65103) ~ bgp_community ) then bgp_med = 2;
                if ( site = "TIS" && (RSasn,65101) ~ bgp_community ) then bgp_med = 4;
                if ( site = "TIS" && (RSasn,65102) ~ bgp_community ) then bgp_med = 2;
        }

        if (dbg=1) then print "DEBUG ",peeras,": ACCEPT ",net.ip,"/",net.len," from ",from," ",bgp_community," - ",bgp_ext_community;

        return true;

}


### ETH0 BFD configuration
protocol bfd rs_bfd {
        interface "eth0" {
        interval 400ms;
        multiplier 5;
        passive on;
        };
}

### GR-IX Route Server Client Template
template bgp grix_rs_client {
        local as RSasn;
        source address RSaddress;
        strict bind yes;
        rs client;			# Preserve AS-PATH, next-hop and MED
        passive on;			# Do not initiate bgp sessions
	prefer older on;		# Do not bread ties through IDs; keep the older (more stable) route
	interpret communities off;	# Do not respect NO-EXPORT and NO-ADVERTISE (and preserve them in the outgoing advertisements)
	graceful restart on;		# Participate in graceful restart recovery
	error wait time 60,43200;
}

ipv<?= $t->router->protocol ?> table master

############### END OF COMMON CONFIG, PER MEMBER CONFIGURATION FOLLOWS ###############
<?php
// uncomment the following line to get a look of what's inside
// the $t->router array.
// echo var_export($t->router,true)
?>
