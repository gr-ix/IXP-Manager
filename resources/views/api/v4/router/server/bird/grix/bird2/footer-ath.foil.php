### Manual Customers

### Full name: AS112 - AS number: 112 - IPv4/6 address: 176.126.38.112 
### Full location name: Ethniko Idryma Erevnon - Location shortname: EIE - Location tag: eie
### Peering macro: N/A - Maximum # of prefixes: N/A - BGP MD5 secret:

ipv<?= $t->router->protocol() ?> table t_as112;

filter peer_as112_to_master
prefix set as112_prefixes;
int set as112_ASNs;
{
        # Basic prefix validation. Do not propagate funny prefixes.
        # Check first AS in AS-PATH, esp to prevent route servers peering to each other
        if !(avoid_martians()) then reject;
        if (bgp_path.first != 112 ) then reject;


        # Check AS-PATH against the appropriate IRRDB macro
        as112_ASNs = [ 112 ];

        if !(bgp_path.last ~ as112_ASNs ) then reject "REJECTING ",net.ip,"/",net.len," received by as112, AS112: Origin AS",bgp_path.last," not in AS112 MACRO";

        # Check each prefix against the route objects originating from the ASNs in the appropriate IRRDB macro
<?php if( $t->router->protocol() == 4 ): ?>
        as112_prefixes = [ 192.175.48.0/24 ];

<?php else: ?>
        as112_prefixes = [ 2620:4f:8000::/48 ];

<?php endif; ?>
        if ! (net ~ as112_prefixes ) then reject "REJECTING ",net.ip,"/",net.len," received by as112, AS112: No route object with origin AS",bgp_path.last," for this prefix";

        # Prepend 1,2,3 times, if (RSasn,65001), (RSasn,65002), (RSasn,65003) have been set
        if (RSasn,65501) ~ bgp_community then bgp_path.prepend(RSasn);
        if (RSasn,65502) ~ bgp_community then { bgp_path.prepend(RSasn); bgp_path.prepend(RSasn); }
        if (RSasn,65503) ~ bgp_community then { bgp_path.prepend(RSasn); bgp_path.prepend(RSasn); bgp_path.prepend(RSasn); }

        # Mark prefixies with "site" community
        bgp_community.add ((RSasn,65101));

        # Done filtering & manipulation. Accept!
        accept;
}

filter filter_bgp_as112
{
    accept;
}

<?php if( $t->router->protocol() == 4 ): ?>
protocol pipe {
        description "Pipe for as112 (AS112 - 176.126.38.112)";
        table master;
        peer table t_as112;
        #mode transparent;
        import filter peer_as112_to_master;
        export where master_to_as(112,"EIE",0);
}

protocol bgp b_as112 from grix_rs_client {
        description "BGP for as112 (AS112 - 176.126.38.112)";
        neighbor 176.126.38.112 as 112;
        ipv4 {
            table t_as112;
            import limit 300;
            import filter filter_bgp_as112;
            export all;
        };
        bfd on; # This should be configured for specific peers in the future.
}
<?php else: ?>
protocol pipe {
        description "Pipe for as112 (AS112 - 2001:7f8:6e::112)";
        table master;
        peer table t_as112;
        #mode transparent;
        import filter peer_as112_to_master;
        export where master_to_as(112,"EIE",0);
}

protocol bgp b_as112 from grix_rs_client {
        description "BGP for as112 (AS112 - 2001:7f8:6e::112)";
        neighbor 2001:7f8:6e::112 as 112;
        ipv6 {
            table t_as112;
            import limit 300;
            import filter filter_bgp_as112;
            export all;
        };
        bfd on; # This should be configured for specific peers in the future.
}
<?php endif; ?>



