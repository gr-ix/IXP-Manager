<?php
    /** @var Foil\Template\Template $t */
    $this->layout( 'layouts/ixpv4' );
?>

<?php $this->section( 'page-header-preamble' ) ?>
   Colocation at EIE/NHRF
<?php $this->append() ?>

<?php $this->section( 'content' ) ?>


<div class="alert alert-info">
    <h4 align="center">
        <span style="margin-left:1em;margin-right:1em;"><a href="<?= route( 'content', [ 'priv' => 1, 'page' => 'colocation#physical_access' ] ) ?>">Physical Access</a></span>
	|<span style="margin-left:1em;margin-right:1em;"><a href="<?= route( 'content', [ 'priv' => 1, 'page' => 'colocation#ip_camera_access' ] ) ?>">Access to IP camera</a></span>
        |<span style="margin-left:1em;margin-right:1em;"><a href="<?= route( 'content', [ 'priv' => 1, 'page' => 'colocation#oob' ] ) ?>">Out Of Band access</a></span>
    </h4>
</div>

<h4 style="margin-top:2em;"><a id="physical_access" style="color:#333;text-decoration:none;padding-top:2.6em;">Physical Access</a></h4>

<p><em>Η παρακάτω διαδικασία ορίζεται από το ΕΙΕ/ΕΚΤ σε συνεργασία με την ΕΔΕΤ</em></p>

<p>
Η πρόσβαση στο κόμβο <?= config('identity.orgname' ) ?> είναι δυνατή σε εργάσιμες ημέρες
και ώρες, έπειτα από συννενόηση με την ΟΜΑΔΑ ΔΙΑΧΕΙΡΙΣΗΣ του
<?= config('identity.orgname' ) ?>. Ειδικά για την αντιμετώπιση έκτακτων περιστατικών
υπάρχει δυνατότητα πρόσβασης και κατά τις μη εργάσιμες ημέρες και ώρες, μεσω του
24ώρου τηλεφώνου επικοινωνίας.
</p>

<p>
Το κάθε μέλος του <?= config('identity.orgname' ) ?> θα πρέπει να δηλώσει στην αρχή κάθε ημερολογιακού έτους μέχρι
πέντε (5) ονόματα συνεργατών του καθώς και πλήρη στοιχεία επικοινωνίας, οι
οποίοι θα έχουν δικαίωμα πρόσβασης στο κόμβο κατά το τρέχον ημερολογιακό έτος
με τη συνοδεία των συνεργατών της ΕΔΕΤ.
</p>

<p>
Για προγραμματισμένες εργασίες σε εργάσιμες ημέρες και ώρες απαιτείται από
κοινού συμφωνία του φορέα με τους Υπεύθυνους Κόμβου για την ημέρα και ώρα
επίσκεψης στο κόμβο. Τα σχετικά αιτήματα υποβάλλονται μέσω της ομάδας helpdesk.
</p>

<p>
Για έκτακτες εργασίες σε μη εργάσιμες ημέρες και ώρες  προς επίλυση
προβλημάτων που έχουν συνέπεια την διακοπή ή την σημαντική υποβάθμιση των
υπηρεσιών προς τα μέλη του <?= config('identity.orgname' ) ?> θα είναι δυνατή η πρόσβαση στο κόμβο μέσα σε εύλογο
χρονικό διάστημα.
</p>

<p><a href="<?= route( 'content', [ 'priv' => 1, 'page' => 'colocation#top' ] ) ?>" style="text-decoration:none">[top]</a></p>

<h4 style="margin-top:2em;"><a id="ip_camera_access" style="color:#333;text-decoration:none;padding-top:2.6em;">Access to IP camera</a></h4>

<p>Η διεύθυνση στη δικτυακή κάμερα <a href="https://grix-camera.grnet.gr">grix-camera.grnet.gr</a> είναι δυνατή με χρήση IPv4/v6 πάνω από HTTPS (443).</p>

<p>Οι κωδικοί πρόσβασης είναι grix/grixcam.</p>

<p>Η πρόσβαση θα είναι δυνατή από τα δίκτυα διαχείρισης των μελών του <?= config('identity.orgname' ) ?>.</p>

<p><a href="<?= route( 'content', [ 'priv' => 1, 'page' => 'colocation#top' ] ) ?>" style="text-decoration:none">[top]</a></p>

<h4 style="margin-top:2em;"><a id="oob" style="color:#333;text-decoration:none;padding-top:2.6em;">Out Of Band access</a></h4>

<h5 style="margin-top:2em;"><a id="terminal_servers" style="color:#333;text-decoration:none;padding-top:2.6em;">Terminal Servers</a></h5>

<p>Οι πελάτες του <?= config('identity.orgname' ) ?> μπορούν να συνδεθούν μέσω terminal server σειριακά στο δικτυακό εξοπλισμό που στεγάζεται στο χώρο του ΕΚΤ/ΕΙΕ. Για το σκοπό αυτό χρησιμοποιούνται οι παρακάτω συσκευές:</p>

<table class="table" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Name</th>
      <th>Model</th>
      <th>IP</th>
      <th>Access</th>
      <th>Escape</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>ts1.gr-ix.gr</code></td>
      <td>Digi TS16</td>
      <td><code>83.212.3.38</code></td>
      <td>ssh, telnet</td>
      <td>ctrl+F</td>
    </tr>
    <tr>
      <td><code>ts2.gr-ix.gr</code></td>
      <td>Digi TS16</td>
      <td><code>83.212.3.37</code></td>
      <td>ssh, telnet</td>
      <td>ctrl+F</td>
    </tr>
  </tbody>
</table>

<h6 style="margin-top: 2em;">ts1.gr-ix.gr</h6>

<table class="table" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Port</th>
      <th>Connected</th>
      <th>Comments</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>σύνδεση με τον eie-ts1</td>
      <td></td>
    </tr>
    <tr>
      <td>2</td>
      <td>σύνδεση με τον ts2.gr-ix.gr</td>
      <td></td>
    </tr>
    <tr>
      <td>3</td>
      <td>cyta-sw</td>
      <td></td>
    </tr>
    <tr>
      <td>4</td>
      <td>hol-sw</td>
      <td></td>
    </tr>
    <tr>
      <td>5</td>
      <td>vodafone-pir</td>
      <td></td>
    </tr>
    <tr>
      <td>6</td>
      <td>wind-sw1</td>
      <td></td>
    </tr>
    <tr>
      <td>7</td>
      <td>wind-sw2</td>
      <td></td>
    </tr>
    <tr>
      <td>8</td>
      <td>forthnet-sw1</td>
      <td></td>
    </tr>
    <tr>
      <td>9</td>
      <td>forthnet-sw2</td>
      <td></td>
    </tr>
    <tr>
      <td>10</td>
      <td>ontelecoms-sw1</td>
      <td></td>
    </tr>
    <tr>
      <td>11</td>
      <td>ontelecoms-sw2</td>
      <td></td>
    </tr>
    <tr>
      <td>12</td>
      <td>ote-sw1</td>
      <td></td>
    </tr>
    <tr>
      <td>13</td>
      <td>ote-sw2</td>
      <td></td>
    </tr>
    <tr>
      <td>14</td>
      <td>r.gr-ix.gr</td>
      <td></td>
    </tr>
    <tr>
      <td>15</td>
      <td>vodafone-kif</td>
      <td></td>
    </tr>
    <tr>
      <td>16</td>
      <td>orange-sw</td>
      <td></td>
    </tr>
  </tbody>
</table>

<h6 style="margin-top: 2em;">ts2.gr-ix.gr</h6>

<table class="table" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Port</th>
      <th>Connected</th>
      <th>Comments</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>σύνδεση με τον ts1.gr-ix.gr</td>
      <td></td>
    </tr>
    <tr>
      <td>2</td>
      <td>eie-sw.gr-ix_MSM-A</td>
      <td></td>
    </tr>
    <tr>
      <td>3</td>
      <td>eie-sw.gr-ix_MSM-B</td>
      <td></td>
    </tr>
    <tr>
      <td>4</td>
      <td>mlab-sw</td>
      <td></td>
    </tr>
    <tr>
      <td>5</td>
      <td>med-sw</td>
      <td></td>
    </tr>
    <tr>
      <td>6</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>7</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>8</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>9</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>10</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>11</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>12</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>13</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>14</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>15</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>16</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

<h5 style="margin-top:2em;"><a id="managed_pdu" style="color:#333;text-decoration:none;padding-top:2.6em;">Managed PDU</a></h5>

<p>Η πελάτες του <?= config('identity.orgname' ) ?> έχουν τη δυνατότητα σύνδεσης του εξοπλισμού τους σε διαχειριζόμενα πολύμπριζα ώστε να καταστεί δυνατή η απομακρυσμένη διακοπή/επαναφορά της τροφοδοσίας.</p>

<h6 style="margin-top: 2em;">Οδηγίες σύνδεσης/χρήσης</h6>

<ol>
  <li>ssh στο αντίστοιχο πολύμπριζο</li>
  <li>στο menu Control Console επιλέγουμε 1-Device Manager</li>
  <li>στη συνέχεια επιλέγουμε 2-Outlet Managemnt και έπειτα 1-Outlet Control/Configuration. Επιλέγουμε την αντίστοιχη έξοδο (outlet) όπου εμφανίζονται δύο επιλογές 1- Immediate On 2- Immediate Off</li>
</ol>

<h6 style="margin-top: 2em;">Λίστα πολύμπριζον</h6>

<table class="table" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Name</th>
      <th>Model</th>
      <th>IP</th>
      <th>Access</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>power1.gr-ix.gr</td>
      <td>AP7953</td>
      <td><code>83.212.3.39</code></td>
      <td>ssh</td>
    </tr>
    <tr>
      <td>power2.gr-ix.gr</td>
      <td>AP7953</td>
      <td><code>83.212.3.40</code></td>
      <td>ssh</td>
    </tr>
    <tr>
      <td>power3.gr-ix.gr</td>
      <td>AP7953</td>
      <td><code>83.212.3.41</code></td>
      <td>ssh</td>
    </tr>
    <tr>
      <td>power4.gr-ix.gr</td>
      <td>AP7953</td>
      <td><code>83.212.3.42</code></td>
      <td>ssh</td>
    </tr>
  </tbody>
</table>

<h6 style="margin-top: 2em;">power1.gr-ix.gr</h6>

<table class="table" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Port</th>
      <th>Connected</th>
      <th>Comments</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>hol-sw1_A</td>
      <td></td>
    </tr>
    <tr>
      <td>2</td>
      <td>ote-sw1_A</td>
      <td></td>
    </tr>
    <tr>
      <td>3</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>4</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>5</td>
      <td>ontelecoms-sw2</td>
      <td></td>
    </tr>
    <tr>
      <td>6</td>
      <td>wind-sw1</td>
      <td></td>
    </tr>
    <tr>
      <td>7</td>
      <td>vodafone-sw1</td>
      <td></td>
    </tr>
    <tr>
      <td>8</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>9</td>
      <td>ote-sw2_A</td>
      <td></td>
    </tr>
    <tr>
      <td>10</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>11</td>
      <td>cyta-sw_A</td>
      <td></td>
    </tr>
    <tr>
      <td>12</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>13</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>14</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>15</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>16</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>17</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>18</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>19</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>20</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>21</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>22</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>23</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>24</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

<h6 style="margin-top: 2em;">power2.gr-ix.gr</h6>

<table class="table" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Port</th>
      <th>Connected</th>
      <th>Comments</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>hol-sw1_B</td>
      <td></td>
    </tr>
    <tr>
      <td>2</td>
      <td>vodafone-sw2</td>
      <td></td>
    </tr>
    <tr>
      <td>3</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>4</td>
      <td>orange-sw</td>
      <td></td>
    </tr>
    <tr>
      <td>5</td>
      <td>ontelecoms-sw1</td>
      <td></td>
    </tr>
    <tr>
      <td>6</td>
      <td>wind-sw2</td>
      <td></td>
    </tr>
    <tr>
      <td>7</td>
      <td>forthnet-sw1</td>
      <td></td>
    </tr>
    <tr>
      <td>8</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>9</td>
      <td>ote-sw1_B</td>
      <td></td>
    </tr>
    <tr>
      <td>10</td>
      <td>forthnet-sw2</td>
      <td></td>
    </tr>
    <tr>
      <td>11</td>
      <td>ote-sw2_B</td>
      <td></td>
    </tr>
    <tr>
      <td>12</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>13</td>
      <td>cyta-sw_B</td>
      <td></td>
    </tr>
    <tr>
      <td>14</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>15</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>16</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>17</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>18</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>19</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>20</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>21</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>22</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>23</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>24</td>
      <td>med-sw</td>
      <td></td>
    </tr>
  </tbody>
</table>

<h6 style="margin-top: 2em;">power3.gr-ix.gr</h6>

<table class="table" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Port</th>
      <th>Connected</th>
      <th>Comments</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>ts2.gr-ix.gr</td>
      <td></td>
    </tr>
    <tr>
      <td>2</td>
      <td>r.gr-ix.gr_A</td>
      <td></td>
    </tr>
    <tr>
      <td>3</td>
      <td>broadbandtest-drac_A</td>
      <td></td>
    </tr>
    <tr>
      <td>4</td>
      <td>mlab3.ath02_A</td>
      <td></td>
    </tr>
    <tr>
      <td>5</td>
      <td>mlab2.ath02_A</td>
      <td></td>
    </tr>
    <tr>
      <td>6</td>
      <td>mlab1.ath02_A</td>
      <td></td>
    </tr>
    <tr>
      <td>7</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>8</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>9</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>10</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>11</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>12</td>
      <td>mlab-sw</td>
      <td></td>
    </tr>
    <tr>
      <td>13</td>
      <td>mlab1.ath01_A</td>
      <td></td>
    </tr>
    <tr>
      <td>14</td>
      <td>mlab2.ath01_A</td>
      <td></td>
    </tr>
    <tr>
      <td>15</td>
      <td>mlab3.ath01_A</td>
      <td></td>
    </tr>
    <tr>
      <td>16</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>17</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>18</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>19</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>20</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>21</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>22</td>
      <td>grdns_A</td>
      <td></td>
    </tr>
    <tr>
      <td>23</td>
      <td>eie-sw.gr-ix.gr_B</td>
      <td></td>
    </tr>
    <tr>
      <td>24</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

<h6 style="margin-top: 2em;">power4.gr-ix.gr</h6>

<table class="table" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Port</th>
      <th>Connected</th>
      <th>Comments</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>ts1.gr-ix.gr</td>
      <td></td>
    </tr>
    <tr>
      <td>2</td>
      <td>r.gr-ix.gr_B</td>
      <td></td>
    </tr>
    <tr>
      <td>3</td>
      <td>broadbandtest-drac_B</td>
      <td></td>
    </tr>
    <tr>
      <td>4</td>
      <td>mlab3.ath02_B</td>
      <td></td>
    </tr>
    <tr>
      <td>5</td>
      <td>mlab2.ath02_B</td>
      <td></td>
    </tr>
    <tr>
      <td>6</td>
      <td>mlab1.ath02_B</td>
      <td></td>
    </tr>
    <tr>
      <td>7</td>
      <td>speedtest-server</td>
      <td></td>
    </tr>
    <tr>
      <td>8</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>9</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>10</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>11</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>12</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>13</td>
      <td>mlab1.ath01_B</td>
      <td></td>
    </tr>
    <tr>
      <td>14</td>
      <td>mlab2.ath01_B</td>
      <td></td>
    </tr>
    <tr>
      <td>15</td>
      <td>mlab3.ath01_B</td>
      <td></td>
    </tr>
    <tr>
      <td>16</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>17</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>18</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>19</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>20</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>21</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>22</td>
      <td>grdns_B</td>
      <td></td>
    </tr>
    <tr>
      <td>23</td>
      <td>eie-sw.gr-ix.gr_A</td>
      <td></td>
    </tr>
    <tr>
      <td>24</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

<p><a href="<?= route( 'content', [ 'priv' => 1, 'page' => 'colocation#top' ] ) ?>" style="text-decoration:none">[top]</a></p>

<?php $this->append() ?>
