


<?php $this->section('content') ?>



<?php
    /** @var Foil\Template\Template $t */
    $this->layout( 'layouts/ixpv4' );
?>

<?php $this->section( 'page-header-preamble' ) ?>
Mailing Lists
<?php $this->append() ?>

<?php $this->section( 'content' ) ?>

<div class="well" style="width: 60%;">

<p><b>Open mailing lists for discussions between GR-IX and its members or among members</b></p>

<p>
Administrative contacts (all members) : <span style="float:right;"><a href="mailto:members-admin@gr-ix.gr">members-admin@gr-ix.gr</a></span><br>
Administrative contacts (GR-IX::Athens members) : <span style="float:right;"><a href="mailto:members-admin@ath.gr-ix.gr">members-admin@ath.gr-ix.gr</a></span><br>
Administrative contacts (GR-IX::Thessaloniki members) : <span style="float:right;"><a href="mailto:members-admin@thess.gr-ix.gr">members-admin@thess.gr-ix.gr</a></span>

<p>
Technical contacts (all members)                      : <span style="float:right;"><a href="mailto:members-tech@gr-ix.gr">members-tech@gr-ix.gr</a></span><br>
Technical contacts (GR-IX::Athens members)            : <span style="float:right;"><a href="mailto:members-tech@ath.gr-ix.gr">members-tech@ath.gr-ix.gr</a></span><br>
Technical contacts (GR-IX::Thessaloniki members)      : <span style="float:right;"><a href="mailto:members-tech@thess.gr-ix.gr">members-tech@thess.gr-ix.gr</a></span></p>

<p><b>Closed lists for tickets (outages, maintenance etc) – to be used only by GR-IX stuff</b></p>

<p>
All members' NOCs                 : <span style="float:right;"><a href="mailto:members-nocs@gr-ix.gr">members-nocs@gr-ix.gr</a></span><br>
GR-IX::Athens members' NOCs       : <span style="float:right;"><a href="mailto:members-nocs@ath.gr-ix.gr">members-nocs@ath.gr-ix.gr</a></span><br>
GR-IX::Thessaloniki members' NOCs : <span style="float:right;"><a href="mailto:members-nocs@thess.gr-ix.gr">members-nocs@thess.gr-ix.gr</a></span><br>

</div>

<?php $this->append() ?>
