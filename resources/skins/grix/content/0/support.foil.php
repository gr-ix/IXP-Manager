<?php
    /** @var Foil\Template\Template $t */
    $this->layout( 'layouts/ixpv4' );
?>

<?php $this->section( 'title' ) ?>
    GR-IX Technical Support and Contact Information
<?php $this->append() ?>


<?php $this->section( 'content' ) ?>


<div class="alert alert-info">
<h4 align="center">
    Technical Support: <a href="mailto:helpdesk@gr-ix.gr">helpdesk@gr-ix.gr</a>
    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    NOC: <a href="mailto:noc@gr-ix.gr">noc@gr-ix.gr</a>
    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    Information/Sales/Billing: <a href="mailto:info@gr-ix.gr">info@gr-ix.gr</a>
</h4>
</div>

<div class="well">

    <table border="0" align="center">
    <tr>
        <td></td>
        <td align="right"><strong>NOC Hours:</strong></td>
        <td></td>
        <td align="left" style="padding-left:1em;">09:00 &ndash; 20:00 EEST (weekdays)</td>
    </tr>
    <tr>
        <td></td>
        <td align="right"><strong>NOC Telephone:</strong></td>
        <td></td>
        <td align="left" style="padding-left:1em;">+30 210-7471090</td>
    </tr>
    <tr>
        <td></td>
        <td align="right"><strong>Email:</strong></td>
        <td></td>
        <td align="left" style="padding-left:1em;"><a href="mailto:noc@gr-ix.gr">noc@gr-ix.gr</a></td>
    </tr>
    <tr>
        <td colspan="4"><hr/></td>
    </tr>
    <tr>
        <td></td>
        <td align="right"><strong>Helpdesk Hours:</strong></td>
        <td></td>
        <td align="left" style="padding-left:1em;">08:00 &ndash; 20:00 EEST (weekdays), 09:00 &ndash; 17:00 EEST (weekends)</td>
    </tr>
    <tr>
        <td></td>
        <td align="right"><strong>Helpdesk Telephone:</strong></td>
        <td></td>
        <td align="left" style="padding-left:1em;">800-11-47638 (800-11-GRNET) (toll-free) or +30-210-9569181 (from mobile phones or abroad)</td>
    </tr>
    <tr>
        <td></td>
        <td align="right"><strong>Email Helpdesk:</strong></td>
        <td></td>
        <td align="left" style="padding-left:1em;"><a href="mailto:helpdesk@gr-ix.gr">helpdesk@gr-ix.gr</a></td>
    </tr>
    <tr>
        <td colspan="4"><hr/></td>
    </tr>
    </table>
</div>

<?php $this->append() ?>
