<?php
/** @var Foil\Template\Template $t */
/** @var $t->active */

$this->layout( 'layouts/ixpv4' );
?>

<?php $this->section( 'page-header-preamble' ) ?>
    Customers / Links for mail
<?php $this->append() ?>

<?php $this->section( 'page-header-postamble' ) ?>


<?php $this->append() ?>

<?php $this->section('content') ?>

        <table id='customer-list' class="table collapse table-striped no-wrap responsive tw-shadow-md" style="width:100%" >
            <thead class="thead-dark">
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                    </th>
                    <th>
                    </th>
                    <th>
                    </th>
                    <th>
                    </th>
                    <th>
                    </th>
                    <th>
                    </th>
                    <th>
                    </th>
                </tr>
            <thead>
            <tbody>
            <?php foreach( $t->custs as $c ):
                /** @var Entities\Customer $c */
                $d = $t->data[ $c->id ];
                ?>

                <tr>
                    <td>
                            <?= $t->ee( $c->name ) ?>
                    </td>
                    <td>
                            <?php if ( $d['text_type'] == 'Full' ) { ?>
                              <a href="mailto:<?= urlencode($d['to']) ?>?cc=<?= urlencode($d['cc']) ?>&subject=<?= rawurlencode($d['subject']) ?>&body=<?= rawurlencode($d['body']) ?>">services & cost analysis</a>
                            <?php } ?>
                    </td>
                    <td>
                            <?php if ( $d['text_type'] == 'Full' ) { ?>
                              <a href="mailto:<?= urlencode($d['to2']) ?>?cc=<?= urlencode($d['cc']) ?>&subject=<?= rawurlencode($d['subject2']) ?>&body=<?= rawurlencode($d['body2']) ?>">GR-IX e-invoice</a>
                            <?php } ?>
                    </td>
                    <td>
                            <?php if ( $d['text_type'] == 'Full' ) { ?>
                              <a href="mailto:<?= urlencode($d['to7']) ?>?cc=<?= urlencode($d['cc7']) ?>&subject=<?= rawurlencode($d['subject7']) ?>&body=<?= rawurlencode($d['body7']) ?>">Update Contacts</a>
                            <?php } ?>
                    </td>
                    <td>
                            <?php if ( $d['text_type'] == 'Full' ) { ?>
                              <a href="mailto:<?= urlencode($d['to3']) ?>">Admin+Billing+Invoice</a>
                            <?php } ?>
		            </td>

                    <td>
                              <a href="mailto:<?= urlencode($d['to4']) ?>">Admin+Tech</a>
                    </td>
                    <td>
                              <a href="mailto:<?= urlencode($d['to5']) ?>">Admin</a>
                    </td>
                    <td>
                              <a href="mailto:<?= urlencode($d['to6']) ?>">Tech+NOC</a>
                    </td>
                    <td>
                      <?= $t->insert( 'customer/list-type',   [ 'cust' => $c ] ) ?>
                    </td>
                    <td>
                      <?= $t->insert( 'customer/list-status',   [ 'cust' => $c ] ) ?>
                    </td>
                </tr>

            <?php endforeach;?>
            <tbody>
        </table>



<?php $this->append() ?>

<?php $this->section( 'scripts' ) ?>
    <script>
        $(document).ready( function() {

            $( '#customer-list' ).show();

            $('#customer-list').DataTable( {
                responsive: true,
                columnDefs: [
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: -1 }
                ],
            } );

        });
    </script>
<?php $this->append() ?>
