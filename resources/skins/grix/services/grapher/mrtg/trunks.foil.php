<?php

    // MRTG Configuration Templates
    //
    // This is v4 maintaining legacy support for manual trunk graphs.
    // Please see: https://github.com/inex/IXP-Manager/wiki/MRTG---Traffic-Graphs
    //
    // This will be phased out / automated / managed on the frontend soon
    //
    // You should not need to edit these files - instead use your own custom skins. If
    // you can't effect the changes you need with skinning, consider posting to the mailing
    // list to see if it can be achieved / incorporated.
    //
    // Skinning: https://ixp-manager.readthedocs.org/en/latest/features/skinning.html

?>


### lh-sw.gr-ix.gr to med-sw.gr-ix.gr
#Target[core-lh-med-lan2]: #ae9:public@lh-sw.gr-ix.gr:::::2
#MaxBytes[core-lh-med-lan2]: 10000000000
#Directory[core-lh-med-lan2]: trunks
#Title[core-lh-med-lan2]: Trunk Core - lh-sw.gr-ix.gr med-sw.gr-ix.gr - LAN2
#
### med-sw.gr-ix.gr to eie-sw.gr-ix.gr
#Target[core-med-eie-lan3]: #ae0:public@med-sw.gr-ix.gr:::::2
#MaxBytes[core-med-eie-lan3]: 2500000000
#Directory[core-med-eie-lan3]: trunks
#Title[core-med-eie-lan3]: Trunk Core - med-sw.gr-ix.gr eie-sw.gr-ix.gr - LAN3

## eie-sw2 to lh-sw2
#Target[core-eie-sw2-lh-sw2]: #xe-0/0/23:public@eie-sw2.gr-ix.gr:::::2
#MaxBytes[core-eie-sw2-lh-sw2]: 2500000000
#Directory[core-eie-sw2-lh-sw2]: trunks
#Title[core-eie-sw2-lh-sw2]: Trunk Core - eie-sw2 lh-sw2

## eie-sw2 to eie-sw1
Target[core-eie-sw2-eie-sw1]: #ae4:public@eie-sw2.gr-ix.gr:::::2
MaxBytes[core-eie-sw2-eie-sw1]: 10000000000
Directory[core-eie-sw2-eie-sw1]: trunks
Title[core-eie-sw2-eie-sw1]: Trunk Core - eie-sw2 eie-sw1

## eie-sw2 to eie-sw
#Target[core-eie-sw2-eie-sw]: #xe-0/0/22:public@eie-sw2.gr-ix.gr:::::2
#MaxBytes[core-eie-sw2-eie-sw]: 2500000000
#Directory[core-eie-sw2-eie-sw]: trunks
#Title[core-eie-sw2-eie-sw]: Trunk Core - eie-sw2 eie-sw

## eie-sw1 to tis-sw2
Target[core-eie-sw1-tis-sw2]: #ae47:public@eie-sw1.gr-ix.gr:::::2
MaxBytes[core-eie-sw1-tis-sw2]: 2500000000
Directory[core-eie-sw1-tis-sw2]: trunks
Title[core-eie-sw1-tis-sw2]: Trunk Core - eie-sw1 tis-sw2

## eie-sw1 to lh-sw2
Target[core-eie-sw1-lh-sw2]: #ae46:public@eie-sw1.gr-ix.gr:::::2
MaxBytes[core-eie-sw1-lh-sw2]: 2500000000
Directory[core-eie-sw1-lh-sw2]: trunks
Title[core-eie-sw1-lh-sw2]: Trunk Core - eie-sw1 lh-sw2

## lh-sw3 to lh-sw2
Target[core-lh-sw3-lh-sw2]: #et-0/0/26:public@lh-sw3.gr-ix.gr:::::2
MaxBytes[core-lh-sw3-lh-sw2]: 5000000000
Directory[core-lh-sw3-lh-sw2]: trunks
Title[core-lh-sw3-lh-sw2]: Trunk Core - lh-sw3 lh-sw2

## lh-sw3 to lh-sw1
Target[core-lh-sw3-lh-sw1]: #et-0/0/24:public@lh-sw3.gr-ix.gr:::::2
MaxBytes[core-lh-sw3-lh-sw1]: 5000000000
Directory[core-lh-sw3-lh-sw1]: trunks
Title[core-lh-sw3-lh-sw1]: Trunk Core - lh-sw3 lh-sw1

### lh-sw3 to lh-sw
#Target[core-lh-sw3-lh-sw]: #ae40:public@lh-sw3.gr-ix.gr:::::2
#MaxBytes[core-lh-sw3-lh-sw]: 10000000000
#Directory[core-lh-sw3-lh-sw]: trunks
#Title[core-lh-sw3-lh-sw]: Trunk Core - lh-sw3 lh-sw

## lh-sw3 to tis-sw3
#Target[core-lh-sw3-tis-sw3]: #ae44:public@lh-sw3.gr-ix.gr:::::2
#MaxBytes[core-lh-sw3-tis-sw3]: 2500000000
#Directory[core-lh-sw3-tis-sw3]: trunks
#Title[core-lh-sw3-tis-sw3]: Trunk Core - lh-sw3 tis-sw3 (lh-sw3 ae44)

## lh-sw3 to tis-sw3 (2)
#Target[core-lh-sw3-tis-sw3-2]: #ae46:public@lh-sw3.gr-ix.gr:::::2
#MaxBytes[core-lh-sw3-tis-sw3-2]: 2500000000
#Directory[core-lh-sw3-tis-sw3-2]: trunks
#Title[core-lh-sw3-tis-sw3-2]: Trunk Core - lh-sw3 tis-sw3 (lh-sw3 ae46)

## lh-sw3 to tis-sw3 (3)
#Target[core-lh-sw3-tis-sw3-3]: #ae47:public@lh-sw3.gr-ix.gr:::::2
#MaxBytes[core-lh-sw3-tis-sw3-3]: 2500000000
#Directory[core-lh-sw3-tis-sw3-3]: trunks
#Title[core-lh-sw3-tis-sw3-3]: Trunk Core - lh-sw3 tis-sw3 (lh-sw3 ae47)

## lh-sw3 to tis-sw3 (4)
#Target[core-lh-sw3-tis-sw3-4]: #ae48:public@lh-sw3.gr-ix.gr:::::2
#MaxBytes[core-lh-sw3-tis-sw3-4]: 2500000000
#Directory[core-lh-sw3-tis-sw3-4]: trunks
#Title[core-lh-sw3-tis-sw3-4]: Trunk Core - lh-sw3 tis-sw3 (lh-sw3 ae48)

## lh-sw3 to lh-sw1
#Target[core-lh-sw3-lh-sw1-bb]: #et-0/0/25:public@lh-sw3.gr-ix.gr:::::2
#MaxBytes[core-lh-sw3-lh-sw1-bb]: 5000000000
#Directory[core-lh-sw3-lh-sw1-bb]: trunks
#Title[core-lh-sw3-lh-sw1-bb]: Trunk Core - lh-sw3 lh-sw1 (backbone)

## lh-sw3 to lh-sw2
#Target[core-lh-sw3-lh-sw2-bb]: #et-0/0/27:public@lh-sw3.gr-ix.gr:::::2
#MaxBytes[core-lh-sw3-lh-sw2-bb]: 5000000000
#Directory[core-lh-sw3-lh-sw2-bb]: trunks
#Title[core-lh-sw3-lh-sw2-bb]: Trunk Core - lh-sw3 lh-sw2 (backbone)

## lh-sw2 to lh-sw1
Target[core-lh-sw2-lh-sw1]: #ae48:public@lh-sw2.gr-ix.gr:::::2
MaxBytes[core-lh-sw2-lh-sw1]: 25000000000
Directory[core-lh-sw2-lh-sw1]: trunks
Title[core-lh-sw2-lh-sw1]: Trunk Core - lh-sw2 lh-sw1

## tis-sw3 to tis-sw2
Target[core-tis-sw3-tis-sw2]: #et-0/0/26:public@tis-sw3.gr-ix.gr:::::2
MaxBytes[core-tis-sw3-tis-sw2]: 5000000000
Directory[core-tis-sw3-tis-sw2]: trunks
Title[core-tis-sw3-tis-sw2]: Trunk Core - tis-sw3 tis-sw2

## tis-sw3 to tis-sw1
Target[core-tis-sw3-tis-sw1]: #et-0/0/24:public@tis-sw3.gr-ix.gr:::::2
MaxBytes[core-tis-sw3-tis-sw1]: 5000000000
Directory[core-tis-sw3-tis-sw1]: trunks
Title[core-tis-sw3-tis-sw1]: Trunk Core - tis-sw3 tis-sw1

## tis-sw2 to tis-sw1
Target[core-tis-sw2-tis-sw1]: #ae48:public@tis-sw2.gr-ix.gr:::::2
MaxBytes[core-tis-sw2-tis-sw1]: 25000000000
Directory[core-tis-sw2-tis-sw1]: trunks
Title[core-tis-sw2-tis-sw1]: Trunk Core - tis-sw2 tis-sw1

## tis-sw1 to lh-sw1
#Target[core-tis-sw1-lh-sw1]: #ae49:public@tis-sw1.gr-ix.gr:::::2
#MaxBytes[core-tis-sw1-lh-sw1]: 2500000000
#Directory[core-tis-sw1-lh-sw1]: trunks
#Title[core-tis-sw1-lh-sw1]: Trunk Core - tis-sw1 lh-sw1

## tis-sw2 to lh-sw2
#Target[core-tis-sw2-lh-sw2]: #ae49:public@tis-sw2.gr-ix.gr:::::2
#MaxBytes[core-tis-sw2-lh-sw2]: 2500000000
#Directory[core-tis-sw2-lh-sw2]: trunks
#Title[core-tis-sw2-lh-sw2]: Trunk Core - tis-sw2 lh-sw2

## tis-sw3 to med-sw
#Target[core-tis-sw3-med-sw]: #ae40:public@tis-sw3.gr-ix.gr:::::2
#MaxBytes[core-tis-sw3-med-sw]: 10000000000
#Directory[core-tis-sw3-med-sw]: trunks
#Title[core-tis-sw3-med-sw]: Trunk Core - tis-sw3 med-sw

# tis-sw3 to tis-sw1
#Target[core-tis-sw3-tis-sw1-bb]: #et-0/0/25:public@tis-sw3.gr-ix.gr:::::2
#MaxBytes[core-tis-sw3-tis-sw1-bb]: 5000000000
#Directory[core-tis-sw3-tis-sw1-bb]: trunks
#Title[core-tis-sw3-tis-sw1-bb]: Trunk Core - tis-sw3 tis-sw1 (backbone)

# tis-sw3 to tis-sw2
#Target[core-tis-sw3-tis-sw2-bb]: #et-0/0/27:public@tis-sw3.gr-ix.gr:::::2
#MaxBytes[core-tis-sw3-tis-sw2-bb]: 5000000000
#Directory[core-tis-sw3-tis-sw2-bb]: trunks
#Title[core-tis-sw3-tis-sw2-bb]: Trunk Core - tis-sw3 tis-sw2 (backbone)

## snc-sw1 to snc-sw2
#Target[core-snc-sw1-snc-sw2]: #ae1:public@snc-sw1.thess.gr-ix.gr:::::2
#MaxBytes[core-snc-sw1-snc-sw2]: 5000000000
#Directory[core-snc-sw1-snc-sw2]: trunks
#Title[core-snc-sw1-snc-sw2]: Trunk Core - snc-sw1 snc-sw2

## snc-sw2 to snc-sw
#Target[core-snc-sw2-snc-sw]: #xe-0/0/0:public@snc-sw2.thess.gr-ix.gr:::::2
#MaxBytes[core-snc-sw2-snc-sw]: 2500000000
#Directory[core-snc-sw2-snc-sw]: trunks
#Title[core-snc-sw2-snc-sw]: Trunk Core - snc-sw2 snc-sw

## tis-sw1 to lh-sw1 2x100
Target[core-tis-sw1-lh-sw1-2x100]: #ae40:public@tis-sw1.gr-ix.gr:::::2
MaxBytes[core-tis-sw1-lh-sw1-2x100]: 25000000000
Directory[core-tis-sw1-lh-sw1-2x100]: trunks
Title[core-tis-sw1-lh-sw1-2x100]: Trunk Core - tis-sw1 lh-sw1 (ae40 - 2x100)

## tis-sw1 to lh-sw1 et-0/0/67
Target[core-tis-sw1-lh-sw1-bb1]: #et-0/0/67:public@tis-sw1.gr-ix.gr:::::2
MaxBytes[core-tis-sw1-lh-sw1-bb1]: 12500000000
Directory[core-tis-sw1-lh-sw1-bb1]: trunks
Title[core-tis-sw1-lh-sw1-bb1]: Trunk Core - tis-sw1 lh-sw1 (et-0-0-67 - 1x100)

## tis-sw1 to lh-sw1 et-0/0/71
Target[core-tis-sw1-lh-sw1-bb2]: #et-0/0/71:public@tis-sw1.gr-ix.gr:::::2
MaxBytes[core-tis-sw1-lh-sw1-bb2]: 12500000000
Directory[core-tis-sw1-lh-sw1-bb2]: trunks
Title[core-tis-sw1-lh-sw1-bb2]: Trunk Core - tis-sw1 lh-sw1 (et-0-0-71 - 1x100)

## tis-sw2 to lh-sw2 2x100
Target[core-tis-sw2-lh-sw2-2x100]: #ae40:public@tis-sw2.gr-ix.gr:::::2
MaxBytes[core-tis-sw2-lh-sw2-2x100]: 25000000000
Directory[core-tis-sw2-lh-sw2-2x100]: trunks
Title[core-tis-sw2-lh-sw2-2x100]: Trunk Core - tis-sw2 lh-sw2 (ae40 - 2x100)

## tis-sw2 to lh-sw2 et-0/0/31
Target[core-tis-sw2-lh-sw2-bb3]: #et-0/0/31:public@tis-sw2.gr-ix.gr:::::2
MaxBytes[core-tis-sw2-lh-sw2-bb3]: 12500000000
Directory[core-tis-sw2-lh-sw2-bb3]: trunks
Title[core-tis-sw2-lh-sw2-bb3]: Trunk Core - tis-sw2 lh-sw2 (et-0-0-31 - 1x100)

## tis-sw2 to lh-sw2 et-0/0/35
Target[core-tis-sw2-lh-sw2-bb4]: #et-0/0/35:public@tis-sw2.gr-ix.gr:::::2
MaxBytes[core-tis-sw2-lh-sw2-bb4]: 12500000000
Directory[core-tis-sw2-lh-sw2-bb4]: trunks
Title[core-tis-sw2-lh-sw2-bb4]: Trunk Core - tis-sw2 lh-sw2 (et-0-0-35 - 1x100)

