<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle center-dd-caret d-flex" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Documentation
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="https://www.gr-ix.gr/specs" target="_blank">Technical&nbsp;Specs <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://www.gr-ix.gr/terms" target="_blank">Terms&nbsp;of&nbsp;use <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://www.gr-ix.gr/route-servers/">Route&nbsp;Servers <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="<?= route( 'content', [ 'priv' => 1, 'page' => 'mailinglists' ] ) ?>">Mailing&nbsp;Lists</a>
        <a class="dropdown-item" href="<?= route( 'content', [ 'priv' => 1, 'page' => 'colocation' ] ) ?>">Colocation&nbsp;at&nbsp;EIE/NHRF</a>
    </div>
</li>
