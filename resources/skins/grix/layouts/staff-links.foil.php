<?php // Override this file (via skinning) to add customer staff links for ADMINs ?>

<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle center-dd-caret d-flex" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Staff Links
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="https://github.com/inex/IXP-Manager">IXP Manager @ GitHub</a>

        <a class="dropdown-item" href="https://nic.gr-ix.gr/html/dt.html" target="_blank">Traffic Matrices (Athens)<em style="font-size:smaller">(new)</em></a>
        <a class="dropdown-item" href="https://nic.thess.gr-ix.gr/html/dt.html" target="_blank">Traffic Matrices (Thessaloniki)<em style="font-size:smaller">(new)</em></a>
        <a class="dropdown-item" href="<?= config('identity.url') ?>smokeping/smokeping.cgi?target=infra_1.vlan_1" target="_blank">Smokeping (Athens) <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://nic.thess.gr-ix.gr/smokeping/smokeping.cgi?target=infra_2.vlan_20" target="_blank">Smokeping (Thessaloniki) <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://grafana.gr-ix.gr/d/tdR8QDlMz/rs-stats-ipv4?orgId=2" target="_blank">Route Server Stats (IPv4) <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://grafana.gr-ix.gr/d/pqN9XD_Gk/rs-stats-ipv6?orgId=2" target="_blank">Route Server Stats (IPv6) <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://wiki.noc.grnet.gr/gr-ix" target="_blank">GRNET NOC wiki <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://icinga.grnet.gr/icinga/" target="_blank">GRNET Icinga <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://disco.noc.grnet.gr/" target="_blank">GRNET Observium <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://mon.grnet.gr/lg/" target="_blank">GRNET Looking Glass <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://grafana.gr-ix.gr/d/epbGs2Pnk/interfaces-stats-telemetry?orgId=1" target="_blank">Interfaces Stats (Telemetry)<em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://grafana.gr-ix.gr/d/sC_1JZYnz/cpu-memory-temperature?orgId=1" target="_blank">CPU/Memory/Temperature Stats<em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="https://github.com/inex/IXP-Manager" target="_blank">IXP Manager @ GitHub <em style="font-size:smaller">(external)</em></a>
        <a class="dropdown-item" href="<?= url('mailing') ?>">Email Templates</a> </li>
    </div>
</li>
