Dear {{$c->name}},

Please take some time to read this email -- it contains important information regarding your Greek Internet Exchange (GR-IX) membership. Please contact us if any if this information is inaccurate or outdated.

Contents:
1. General Information
2. Invoicing
3. Peering & Technical Information
4. Contacts, roles and users
5. Connections & Ports
6. Route Servers
7. How to contact GR-IX and GR-IX members
8. How GR-IX will contact you

#### 1. General Information

The following information is available to all members:

        Company Name (Trade/Business):  {{$c->name}}
        Company website:                {{$c->corpwww}}

#### 2. Invoicing

@if($c->type == 'Internal' || $c->type == 'Pro-bono' )
Not applicable - GR-IX services are provided to {{$c->name}} for free
@elseif( $c->companyBillingDetail->billingFrequency == 'NOBILLING' )
No billing or billing via a reseller
@else
Invoices will be made to:

        Full company Name:              {{$c->companyRegisteredDetail->registeredName}}
        VAT Number:                     @if($c->companyBillingDetail->vatNumber){{$c->companyBillingDetail->vatNumber}}@endif 
        Company Address:                @if($c->companyRegisteredDetail->address1){{$c->companyRegisteredDetail->address1}}@endif @if($c->companyRegisteredDetail->address2), {{$c->companyRegisteredDetail->address2}}@endif @if($c->companyRegisteredDetail->address3), {{$c->companyRegisteredDetail->address3}}@endif @if($c->companyRegisteredDetail->townCity), {{$c->companyRegisteredDetail->townCity}}@endif @if($c->companyRegisteredDetail->postcode), {{$c->companyRegisteredDetail->postcode}}@endif 
	    Country:                        @if($c->companyRegisteredDetail->country){{$c->companyRegisteredDetail->country}}@endif
@if($c->companyBillingDetail->invoiceMethod == 'POST')
Hard (paper) copies of the invoices will be sent to:
        Primary Billing Contact Name:   @if($c->companyBillingDetail->billingContactName){{$c->companyBillingDetail->billingContactName}}@endif  
        Primary Billing Contact Email:  @if($c->companyBillingDetail->billingEmail){{$c->companyBillingDetail->billingEmail}}@endif  
        Primary Billing Contact Phone:  @if($c->companyBillingDetail->billingTelephone){{$c->companyBillingDetail->billingTelephone}}@endif  
        Primary Billing Address:        @if($c->companyBillingDetail->billingAddress1){{$c->companyBillingDetail->billingAddress1}}@endif  
                                        @if($c->companyBillingDetail->billingAddress2){{$c->companyBillingDetail->billingAddress2}}@endif  
@elseif($c->companyBillingDetail->invoiceMethod == 'EMAIL')
The invoices will be emailed to
        Invoice Email:                  @if($c->companyBillingDetail->invoiceEmail){{$c->companyBillingDetail->invoiceEmail}}@endif 
	@endif 


In addition, the invoices will be emailed to all administrative and billing contacts (see “contacts”).
@endif
#### 3. Peering & Technical Information

The following data are available to all GR-IX members:

        AS Number:                      {{$c->autsys}} 
        Peering policy:                 {{$c->peeringpolicy}} 
        IRR:                            @if($c->irrdb){{$c->irrdbConfig()->get()[0]->source}}@endif 
        AS-SET (v4/v6):                 {{$c->peeringmacro}}@if($c->peeringmacrov6)/{{$c->peeringmacrov6}}@endif 
        Peering email:                  {{$c->peeringemail}} 
        NOC phone:                      {{$c->nocphone}} 
        NOC emergency (24h) phone:      {{$c->noc24hphone}} 
        NOC email address:              {{$c->nocemail}}

#### 4. Contacts. roles and users
***Contacts and Roles***

Please make sure that the following contact list is accurate and updated and that the roles are appropriately assigned:

@foreach($c->contacts()->get()->all() as $contact)

  {{$contact->name}}  
  Contact info: {{$contact->email}}, {{$contact->phone}}@if($contact->phone && $contact->mobile) / @endif {{$contact->mobile}} 
  Roles: @if($contact->contactRoles() )@foreach($contact->contactRoles()->get()->all() as $group)@if($group->type == 'ROLE'){{$group->name}}, @endif @endforeach @else - @endif

@endforeach
 Notes on contacts:
 - The Legal Representatives:
    - represent the member
    - sign on its behalf for GR-IX matters
 - The administrative contacts:
    - can order or change services that affect billing
    - will be contacted for all contractual matters, changes in pricelist or other administrative purpose defined in the contract.
    - will be notified for the invoices that are being issued
    - will be included in the administrative GR-IX mailing list (see below)
 - The technical contacts:
    - can request changes that not affect billing
    - will be contacted for any technical issue or problem
    - will be included in the technical GR-IX mailing list (see below)
 - The billing contacts 
    - receive the invoices
    - will be contacted for all billing matters

***Access to the Portal and user management***

GR-IX portal contains information about your membership and services, graphs and tools, contact and peering details of all other members, documentation and other useful staff. Also through the portal you can self-manage your own contacts and users. The portal can be accessed at:

[<?= config( 'identity.url' ) ?>](<?= config( 'identity.url' ) ?>)  

Contacts that appear with a username have access to the portal. Please make sure that the user list is always updated and strong passwords are used.

@if( count($admins))
Your user accounts are:
@foreach( $admins as $a )
* {{ $a->user->username }} <{{$a->user->email}}>
@endforeach
@else
No user-admin account has been created yet for {{$c->name}} -- please contact us at <?= config( 'identity.email' ) ?> in order to create a user-admin account for you.
@endif

Lost Passwords can be reset at {{ url('password/forgot') }}

#### 5. Connections & Ports

@php
  $d = [];
  $peerings = []; // we'll be using this for the route servers section
  $has_interfaces = false;
  foreach( $c->virtualInterfaces->all() as $vi ) {
    if( count( $vi->physicalInterfaces ) == 0  ) {
      continue;
    }
    $type = $vi->physicalInterfaces[0]->switchPort->type();
    $infra = $vi->vlanInterfaces()->get()[0]->vlan()->get()[0]->infrastructure()->get()[0]->name;
    //$infra = $vi->infrastructure->name;
    $d[ $infra ][ $type ][] = [ 'vi' => $vi ];
    $has_interfaces = true;
  }
  if( $has_interfaces ) {
    print "We have assigned the following IP addresses and switch-ports for your exclusive use:<br>";
  }
  else {
    print "*No addresses and ports*\n";
  }
  $counter = 1;
  foreach( $d as $infra => $di ) {
    // this will make the order of the $di array as having Peering, then Reseller, then Fanouts
    //uksort( $di, function( $a, $b ) { return (( $a == $b )? 0 : (($a == 'Peering')? -1 : ($a == 'Reseller'))? -1 : 1; });

    uksort( $di, function( $a, $b ) 
	{ if ( $a == $b ) 
	  {
	  return 0;
 	  }
	  else 
	  {
	    if ( $a == 'Peering' )
	    {
	    return -1;	
	    }
	    else
	    {
            if ( $a == 'Reseller' )
		return -1;
            else
		return 1;
            }
	   } 
         }); 
    foreach( $di as $type => $dt ) {
      foreach( $dt as $record ) {
        print "<b>Connection $counter: $type at $infra</b><br>";
        $record[ 'counter' ] = $counter;
        $vi = $record[ 'vi' ];
        $vlans = [];
        if( $type == 'Peering' ) { // PEERING
          print "<br>*IP & MAC Addresses*:<br>";
	  $peerings[] = [ 'vi' => $vi, 'counter' => $counter, 'type' => $type, 'infra' => $infra ];
          foreach( $vi->vlanInterfaces()->get() as $vli ) {
	    print "\n";
	    $vlanid = $vli->vlan->id;

            if($vli->ipv6enabled){
              print "IPv6 Address:  ".$vli->ipv6address->address.($netinfo[ $vlanid ][ 6 ][ 'masklen']? '/'.$netinfo[ $vlanid ][ 6 ][ 'masklen']: '' )."<br>";
            }
            else {
              print "          Please contact us at ".config( 'identity.email' )." to enable IPv6<br>";
            }
            print "IPv4 ";
            if($vli->ipv4enabled){
              print "Address:  ".$vli->ipv4address->address.($netinfo[ $vlanid ][ 4 ][ 'masklen']? '/'.$netinfo[ $vlanid ][ 4 ][ 'masklen']: '' )."<br>";
              $l2addrs=[];
              foreach( $vli->layer2addresses->all() as $l2 ) {
                $l2addrs[]=$l2->macFormatted( ':' );
              }
              preg_match('/^(\d)(\d{2})$/', sprintf("%03d",preg_replace( '/^\d+\.\d+\.\d+\./', '', $vli->ipv4address->address ) ), $digits);
              print "Allowed MAC Address: ".join(',',$l2addrs)."<br>";
              $ipv4md5=$vli->ipv4bgpmd5secret;
	      $ipv6md5=$vli->ipv6bgpmd5secret;
            }
            else {
              print "          Please contact us at ".config( 'identity.email' )." to enable IPv4<br>";
            }
	    print "<br>";
          }
        }
        elseif( $type == 'Reseller' ) { // RESELLER
          foreach( $vi->vlanInterfaces()->get() as $vli ) {
            $vlanid = $vli->vlan->id;
            $vlans[] = $vlanid;
          }
          print "Reseller uplink with vlans: ".join(',',$vlans)."<br>";
        }
	else { // Fanout
          //dd($vi->physicalInterfaces->all()[0]->relatedInterface());
          $abbr = $vi->physicalInterfaces->all()[0]->relatedInterface()->virtualInterface->customer->abbreviatedName;
          foreach( $vi->vlanInterfaces()->get() as $vli ) { // this should be only one iteration, but hey
            $vlanid = $vli->vlan->id;
            print "Fanout interface for $abbr (vlan $vlanid)<br>";
          }
	}
        if( count($vi->physicalInterfaces->all()) > 1 ) {
          print "*Physical Ports (LAG):*";
        }
        elseif(  count($vi->physicalInterfaces->all() )== 1 ) {
          print "*Physical Port:*";
        }
        foreach($vi->physicalInterfaces()->get() as $pi) {
          $name = $pi->switchPort->switcher->name;
          $port = $pi->switchPort->name;
          $speed = $pi->speed;
          $duplex = $pi->duplex;
          $location = $pi->switchPort->switcher->cabinet->location->name;
          $colo = $pi->switchPort->switcher->cabinet->name;
          print "<br>";
          print "Switch Port:     $name, $port<br>";
          print "Speed:           $speed Mbps<br>";
          print "Duplex:          $duplex<br>";
          print "Location:        $location<br>";
          print "Colo Cabinet ID: $colo<br>";
          print "<br>";
        }
        $counter++;
        print "<br>";
      }
    }
  }
@endphp

@php
if( $has_interfaces ) {
@endphp
***Notes:***
- <b>Our access lists only allow traffic sourced from the MAC address(es) mentioned above.</b> If you need to use a different MAC address, please contact <helpdesk@gr-ix.gr>.
- If you need a different reverse DNS, please contact <helpdesk@gr-ix.gr>.

@php
}
@endphp

#### 6. Route Servers

The following route servers are available for Athens and Thessaloniki members:

<b>GR-IX::Athens</b>

        Route Server     AS Number     IPv4 Address     IPv6 Address 
        rs0.gr-ix.gr     50745         176.126.38.120   2001:7f8:6e::120
        rs1.gr-ix.gr     50745         176.126.38.121   2001:7f8:6e::121
        rs2.gr-ix.gr     50745         176.126.38.117   2001:7f8:6e::117

<b>GR-IX::Thessaloniki</b>

        Route Server           AS Number     IPv4 Address    IPv6 Address
        rs0.thess.gr-ix.gr     50745         185.1.123.120   2001:7f8:ce::120
        rs1.thess.gr-ix.gr     50745         185.1.123.121   2001:7f8:ce::121

***Depending on your region, you can peer with the corresponding route servers and avoid direct bgp peerings with each member. <b> If you opt to use the route servers, we strongly recommend that each of your routers peers with both route servers in the region</b>***

More information on GR-IX route servers can be found here:
	<https://www.gr-ix.gr/route-servers/>

@php
if( count( $peerings) > 0 ) {
  print "Our route server configuration for your bgp peerings is as follows:<br>";
}
foreach( $peerings as  $p ) {
@endphp
<b>Connection {{ $p['counter'] }} ({{ $p['infra'] }})</b><br>
@php
  foreach( $p['vi']->vlanInterfaces->all() as $vli ) {
@endphp

        IP Address (v4/v6):     @if($vli->ipv4enabled){{$vli->ipv4address->address}} @else - @endif / @if($vli->ipv6enabled){{$vli->ipv6address->address}} @else - @endif 
        Status:                 @if($vli->rsclient)Active @else Inactive @endif 
@if($vli->rsclient)
        BGP setup:              Port 179, passive (you will need to initiate the bgp session) 
        Prefix filtering: 
            Mode:               Strict (exact match prefixes/ origin AS, match AS in AS-PATH) 
            Max. prefixes:      {{$c->maxprefixes}} 
            AS-SET(v4/v6):      @if($c->peeringmacro){{$c->peeringmacro}} @else - @endif @if($c->peeringmacrov6)/{{$c->peeringmacrov6}} @endif 
            IRRDB:              @if($c->irrdb){{$c->irrdbConfig()->get()[0]->source}} @else - @endif  

@else
contact helpdesk@gr-ix.gr to activate
@endif
@php
  }
@endphp
@php
}
@endphp

#### 7. How to contact GR-IX and GR-IX members

<b>Contact GR-IX</b>

All problem reports, requests and inquiries should be sent to GR-IX helpdesk:
        <helpdesk@gr-ix.gr>

For matters that require urgent technical assistance, you may also copy our NOC:
        <noc@gr-ix.gr>

If necessary, GR-IX can be reached by phone. Regular/emergency contacts are listed on the portal:
        <https://portal.gr-ix.gr/static/support>

<b>Contact other members</b> (e.g. for peering requests)

The full list of GR-IX members and their contact information can be found at:
        {{ url('customer/details') }} 

You can send peering requests in an automated fashion via the Peering Manager tool available at our member's portal.
Related information are available at: https://www.gr-ix.gr/peering-manager/.

<b>Mailing Lists</b>

The following open mailing lists can be used to reach the other members:

* Administrative contacts (all members) : <members-admin@gr-ix.gr>

* Administrative contacts (GR-IX::Athens members) : <members-admin@ath.gr-ix.gr>

* Administrative contacts (GR-IX::Thessaloniki members) : <members-admin@thess.gr-ix.gr>

* Technical contacts (all members) : <members-tech@gr-ix.gr>

* Technical contacts (GR-IX::Athens members) : <members-tech@ath.gr-ix.gr>

* Technical contacts (GR-IX::Thessaloniki members) : <members-tech@thess.gr-ix.gr>

#### 8. How GR-IX will contact you

 - Tickets are send out to your NOC email address.
 - Technical announcements, including maintenance notifications, will be emailed to your NOC and to the technical list.
 - Non-technical announcements, including pricelist and term of use updates, will be emailed to the administrative list.

 *Please make sure that <b>the appropriate contacts are defined</b>. Important correspondence may be missed otherwise. <b>Failure to maintain such contacts may breach your contract.</b>*

#### More information

* GR-IX portal: <https://portal.gr-ix.gr/>
* Terms of use: <https://www.gr-ix.gr/terms/>
* Technical specifications: <https://www.gr-ix.gr/specs/>
* Route servers: <https://www.gr-ix.gr/route-servers/>

Regards,

The GR-IX Operations team.
